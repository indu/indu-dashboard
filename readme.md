# Install

1. Create Virtual Environment
2. Install requirements with `pip install -r requirements.txt`	
3. Copy `.env.example` to `.env` and fill in the required fields
4. Run the server with `python wsgi.py`