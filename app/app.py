import os

import dash
import dash_mantine_components as dmc
from dash import dcc
from dash_iconify import DashIconify

app = dash.Dash(__name__, use_pages=True, suppress_callback_exceptions=True)
server = app.server

assets_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'assets')


def create_nav_link(icon, label, href):
    return dcc.Link(
        dmc.Group(
            [
                dmc.ThemeIcon(
                    DashIconify(icon=icon, width=18),
                    size=30,
                    radius=30,
                    variant='light',
                ),
                dmc.Text(label, size='sm', color='gray'),
            ]
        ),
        href=href,
        style={'textDecoration': 'none'},
    )


sidebar = dmc.Navbar(
    fixed=True,
    width={'base': '15%'},
    position={'top': '15vh'},
    height='85vh',
    children=[
        dmc.ScrollArea(
            offsetScrollbars=True,
            type='scroll',
            children=[
                dmc.Stack(
                    children=[
                        create_nav_link(
                            icon='radix-icons:rocket',
                            label='Home',
                            href='/',
                        ),
                    ],
                ),
                dmc.Divider(
                    label='Hydraulic data', style={'marginBottom': 20,
                                                   'marginTop': 20}
                ),
                dmc.Stack(
                    children=[
                        create_nav_link(
                            icon='mdi:chart-line',
                            label=page['title'],
                            href=page['path']
                        )
                        for page in dash.page_registry.values()
                        if page['path'].startswith('/hydraulic')
                    ],
                ),
                dmc.Divider(
                    label='Strain data', style={'marginBottom': 20,
                                                'marginTop': 20}
                ),
                dmc.Stack(
                    children=[
                        create_nav_link(
                            icon='mdi:chart-line',
                            label=page['title'],
                            href=page['path']
                        )
                        for page in dash.page_registry.values()
                        if page['path'].startswith('/strain')
                    ],
                ),
                dmc.Divider(
                    label='Seismic data', style={'marginBottom': 20,
                                                 'marginTop': 20}
                ),
                dmc.Stack(
                    children=[
                        create_nav_link(
                            icon='mdi:chart-line',
                            label=page['title'],
                            href=page['path']
                        )
                        for page in dash.page_registry.values()
                        if page['path'].startswith('/seismic')
                    ],
                ),
                #dmc.Divider(
                #    label='Other plots', style={'marginBottom': 20,
                #                                'marginTop': 20}
                #),
                #dmc.Stack(
                #    children=[
                #        create_nav_link(
                #            icon='mdi:chart-line',
                #            label=page['title'],
                #            href=page['path']
                #        )
                #        for page in dash.page_registry.values()
                #        if page['path'].startswith('/external')
                #    ],
                #),
                dmc.Divider(
                    label='Models', style={'marginBottom': 20, 'marginTop': 20}
                ),
                dmc.Stack(
                    children=[
                        create_nav_link(
                            icon='mdi:analytics',
                            label=page['name'].title(),
                            href=page['path']
                        )
                        for page in dash.page_registry.values()
                        if page['path'].startswith('/models')
                    ],
                ),
            ],
        )
    ],
    style={'marginRight': '80%'},
)

app.layout = dmc.Container(
    [
        dmc.Header(
            height='12%',
            children=[dmc.Center(
                dmc.Image(src='/assets/bedr-kombi.png',
                          style={'maxWidth': '19%'}))],
            style={'backgroundColor': 'gray'},
            fixed=True,
        ),
        sidebar,
        dmc.Container(
            dash.page_container,
            size='lg',
            pt=150,
            style={'marginLeft': '20%'},
        ),
    ],
    fluid=True,
)
