import dash_mantine_components as dmc
from dash import html, register_page

register_page(__name__, path='/', icon='fa-solid:home')

layout = dmc.Container(
    [
        dmc.Title('Real-time analysis and modeling'),
        html.Br(),
        dmc.Text('This web-app allows visualizing the basic hydraulic and seismic observations during experiments at the Bedretto Lab'),
        dmc.Text('as well as the results of an ATLS system to forecast induced seismicity'),
        html.Br(),
        dmc.Divider(variant="solid",size=10),
        html.Br(),
        dmc.Image(src='/assets/bedretto_header.jpg'),
    ]
)
