import dash_mantine_components as dmc
from dash import html, register_page

#register_page(__name__, name='DAS waterfall', title='DAS waterfall')

layout = dmc.Container(
    [
        dmc.Title('Largest event waterfall'),
        html.H2('by Pascal Edme'),
        html.Br(),
        dmc.Image(src='https://polybox.ethz.ch/index.php/s/o0KxO0kuNAm5fx4/download',
                  caption="DAS waterfall plot for largest event in catalog", width='100%'),
    ]
)
