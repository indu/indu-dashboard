import dash_mantine_components as dmc
from dash import html, register_page

#register_page(__name__, name='DTS FBG Pressure', title='DTS FBG Pressure')

layout = dmc.Container(
    [
        dmc.Title('DTS, FBG, and Pressure'),
        html.H2('by Nima Gholizadeh Doonechaly'),
        html.Br(),
        dmc.Image(src='https://polybox.ethz.ch/index.php/s/e58Ljm44a6Mo59y/download?path=%2F&files=FBG_DTS_plot.jpg',
                  caption="DTS in boreholes ST1, MB1, MB8, ST2. FBG for boreholes MB1, MB5, MB7, MB8", width='100%'),

        html.Br(),html.Br(),
        dmc.Divider(variant="solid"),
        html.Br(),html.Br(),

        dmc.Image(src='https://polybox.ethz.ch/index.php/s/e58Ljm44a6Mo59y/download?path=%2F&files=Pressure_plot.jpg',
                  caption="Pressure changes in the last 24 hours in selected intervals", width='100%'),

    ]
)
