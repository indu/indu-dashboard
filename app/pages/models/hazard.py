import dash
import dash_mantine_components as dmc
from dash import Input, Output, State, callback, dcc, html
from hermes_client.clients import ForecastSeriesClient

from app.functions.components import ForecastPlanStyleSelector
from app.functions.data_access import fetch_forecast_modelruns, models_tbl
from app.functions.data_processing import calculate_Pocc_forecast
from app.functions.global_variables import MODELS, RAMSIS_URL  # noqa
from app.functions.plotting_functions import plot_Pocc_forecast

dash.register_page(__name__)

style = {
    'width': '100%',
    'display': 'inline-block',
    'height': '80vh',
    'minHeight': '500px'
}


@callback(Output('graph-container-hz', 'children'),
          State({'type': 'select-fs-ps', 'index': 'fs-hz'}, 'value'),
          Input('select-in-ps', 'value'),
          Input('slider-fc-ps', 'value'),
          Input('cum-rates', 'value'),
          Input({'type': 'uuid-map-fs-ps', 'index': 'fs-hz'}, 'data'),
          prevent_initial_call=True)
def render(forecastseries_id,
           injection_plan_id,
           forecast_id,
           cum_flag,
           uuid_map):

    if not forecast_id or not forecastseries_id:
        return None

    fc_client = ForecastSeriesClient(url=RAMSIS_URL,
                                     forecastseries_id=forecastseries_id)

    forecast_id = uuid_map[str(forecast_id)]

    modelruns = fetch_forecast_modelruns(fc_client, forecast_id)

    model_configs = models_tbl(fc_client)

    injection_plan = next(ip for ip in fc_client.list_injectionplans() if
                          ip['id'] == injection_plan_id)

    models_name = list(model_configs.keys())
    # models_name = MODELS
    models_forecast = [modelruns[next(i for i, fc in enumerate(
        modelruns) if fc['modelconfig_name'] == name and fc[
        'injectionplan_name'] ==
        injection_plan['name'])]['rates_forecast'] for name in models_name]

    models_fit = [modelruns[next(i for i, fc in enumerate(
        modelruns) if fc['modelconfig_name'] == name and fc[
        'injectionplan_name'] ==
        injection_plan['name'])]['rates_fit'] for name in models_name]

    # Calculate Pocc for all model and current injection plan
    Pocc_models = [calculate_Pocc_forecast(
        forecast, fit, cum_flag) for forecast, fit in
        zip(models_forecast, models_fit)]

    fig_hazard_orange = plot_Pocc_forecast(Pocc_models, 'orange', models_name)
    fig_hazard_red = plot_Pocc_forecast(Pocc_models, 'red', models_name)

    return dmc.Container([
        dcc.Graph(figure=fig_hazard_orange, style=style),
        dcc.Graph(figure=fig_hazard_red, style=style),
    ])


# Layout of the page
layout = dmc.Container([
    dmc.Title('Forecast Hazard'),
    html.H2('Probability of occurrance M0 and M1 in forecasting bins'),
    # html.H3('Disclaimer: Magnitudes are MLc '),
    ForecastPlanStyleSelector('hz'),
    dcc.Loading(
        id='loading-graph',
        children=[html.Div(id='graph-container-hz')],
        type='default',
        style={'marginTop': '50px', 'alignSelf': 'flex-start'})
])
