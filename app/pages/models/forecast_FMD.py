import dash
import dash_mantine_components as dmc
from dash import Input, Output, State, callback, dcc, html
from hermes_client.clients import ForecastSeriesClient

from app.functions.components import ForecastPlanSelector
from app.functions.data_access import fetch_forecast_modelruns, models_tbl
from app.functions.data_processing import gutenberg_richter_interp_forecast
from app.functions.global_variables import MODELS, RAMSIS_URL  # noqa
from app.functions.plotting_functions import plot_FMD_forecast

dash.register_page(__name__)

style = {
    'width': '100%',
    'display': 'inline-block',
    'height': '80vh',
    'minHeight': '500px'
}


@callback(Output('graph-container-fmd', 'children'),
          State({'type': 'select-fs-p', 'index': 'fs-fmd'}, 'value'),
          Input('select-in-p', 'value'),
          Input('slider-fc-p', 'value'),
          Input({'type': 'uuid-map-fs-p', 'index': 'fs-fmd'}, 'data'),
          prevent_initial_call=True)
def render(forecastseries_id, injection_plan_id, forecast_id, uuid_map):

    if not forecast_id or not forecastseries_id:
        return None

    fc_client = ForecastSeriesClient(url=RAMSIS_URL,
                                     forecastseries_id=forecastseries_id)
    forecast_id = uuid_map[str(forecast_id)]

    modelruns = fetch_forecast_modelruns(fc_client, forecast_id)

    model_configs = models_tbl(fc_client)
    seismicity = fc_client.get_forecast_seismicity(forecast_id)

    injection_plan = next(ip for ip in fc_client.list_injectionplans() if
                          ip['id'] == injection_plan_id)

    models_name = list(model_configs.keys())
    # models_name = MODELS
    models_fit = [next(fit for fit in modelruns if
                       fit['modelconfig_name'] == name)['rates_fit'] for
                  name in models_name]

    models_forecast = [modelruns[next(i for i, fc in enumerate(
        modelruns) if fc['modelconfig_name'] == name and fc[
            'injectionplan_name'] ==
        injection_plan['name'])]['rates_forecast'] for name in models_name]

    GR_model = [gutenberg_richter_interp_forecast(forecast, fit, seismicity)
                for forecast, fit in zip(models_forecast, models_fit)]

    fig_FMD = plot_FMD_forecast(GR_model, models_name, seismicity)

    return dmc.Container([
        dcc.Graph(figure=fig_FMD, style=style),
    ])


# Layout of the page
layout = dmc.Container([
    dmc.Title('Frequency-Magnitude Distribution Models'),
    # html.H3('Disclaimer: Magnitudes are MLc '),
    ForecastPlanSelector('fmd'),
    dcc.Loading(
        id='loading-graph',
        children=[html.Div(id='graph-container-fmd')],
        type='default',
        style={'marginTop': '50px', 'alignSelf': 'flex-start'})
])
