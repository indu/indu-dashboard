import dash
import dash_mantine_components as dmc
from dash import Input, Output, State, callback, dcc, html
from hermes_client.clients import ForecastSeriesClient

from app.functions.components import ForecastPlanStyleSelector
from app.functions.data_access import fetch_forecast_modelruns
from app.functions.global_variables import RAMSIS_URL
from app.functions.plotting_functions import plot_forecast_rates

dash.register_page(__name__)

style = {
    'width': '100%',
    'display': 'inline-block',
    'height': '80vh',
    'minHeight': '500px'
}


@callback(Output('graph-container-rates', 'children'),
          State({'type': 'select-fs-ps', 'index': 'fs-rates'}, 'value'),
          Input('select-in-ps', 'value'),
          Input('slider-fc-ps', 'value'),
          Input('cum-rates', 'value'),
          Input({'type': 'uuid-map-fs-ps', 'index': 'fs-rates'}, 'data'),
          prevent_initial_call=True)
def render(forecastseries_id,
           injection_plan_id,
           forecast_id,
           cum_flag,
           uuid_map):

    if not forecast_id or not forecastseries_id:
        return None

    fc_client = ForecastSeriesClient(url=RAMSIS_URL,
                                     forecastseries_id=forecastseries_id)

    forecast_id = uuid_map[str(forecast_id)]

    modelruns = fetch_forecast_modelruns(fc_client, forecast_id)

    seismicity = fc_client.get_forecast_seismicity(forecast_id)
    hydraulics = fc_client.get_forecast_injectionwells(forecast_id)

    hydraulics_df = \
        hydraulics[0][fc_client.metadata['model_settings']
                      ['well_section_publicid']].hydraulics

    injection_plan = next(ip for ip in fc_client.list_injectionplans() if
                          ip['id'] == injection_plan_id)

    # extract borehole_hydraulics of injection plan
    injection_plan_hydraulics_df = \
        injection_plan['borehole_hydraulics'][
            fc_client.metadata['model_settings']
            ['well_section_publicid']].hydraulics

    models_name = set(fit['modelconfig_name'] for fit in modelruns)
    models_fit = [next(fit for fit in modelruns if
                       fit['modelconfig_name'] == name)['rates_fit']
                  for name in models_name]

    models_forecast = [modelruns[next(i for i, fc in enumerate(
        modelruns) if fc['modelconfig_name'] == name and fc[
            'injectionplan_name'] ==
        injection_plan['name'])]['rates_forecast'] for name in models_name]

    if cum_flag == 'rates':
        cum = False
    else:
        cum = True

    fig_rate = plot_forecast_rates(seismicity,
                                   hydraulics_df,
                                   models_name,
                                   models_fit,
                                   injection_plan_hydraulics_df,
                                   models_forecast,
                                   cum=cum)

    return dmc.Container([
        dcc.Graph(figure=fig_rate, style=style),
        # dcc.Graph(figure=fig_FMD, style=style),
    ])


# Layout
layout = dmc.Container([
    dmc.Title('Seismicity Forecast Models'),
    # html.H3('Disclaimer: Magnitudes are MLc '),
    ForecastPlanStyleSelector('rates'),
    dcc.Loading(
        id='loading-graph',
        children=[html.Div(id='graph-container-rates')],
        type='default',
        style={'marginTop': '50px', 'alignSelf': 'flex-start'})
])
