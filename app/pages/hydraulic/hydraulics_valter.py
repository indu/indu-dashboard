from datetime import datetime, timedelta, timezone

import dash
import dash_mantine_components as dmc
from dash import Input, Output, State, callback, dcc, html, register_page
from hydws.client import HYDWSDataSource

from app.functions.global_variables import HYDWS_URL,BOREHOLES_SET2
from app.functions.plotting_functions import plotting_hydraulics
from app.functions.fetch_hydraulics import get_sections


register_page(__name__, name='Hydraulics VALTER', title='Hydraulics VALTER')

url_hydraulics = f'{HYDWS_URL}/hydws/v1'

client = HYDWSDataSource(url_hydraulics)

list_out_callback = [Output('graphVALTER'+str(idx),'figure') for idx in range(len(BOREHOLES_SET2))]

@callback(list_out_callback,
          Input('submit-button-state', 'n_clicks'),
          State('date-start', 'value'),
          State('time-start', 'value'),
          State('date-end', 'value'),
          State('time-end', 'value'))
def update_graph(n_clicks,
                 date_start,
                 time_start,
                 date_end,
                 time_end):

    time_start = datetime.strptime(time_start, '%Y-%m-%dT%H:%M:%S')
    time_end = datetime.strptime(time_end, '%Y-%m-%dT%H:%M:%S')
    date_start = datetime.strptime(date_start, '%Y-%m-%d')
    date_end = datetime.strptime(date_end, '%Y-%m-%d')

    start = datetime.combine(date_start.date(), time_start.time())
    end = datetime.combine(date_end.date(), time_end.time())

    figures=[]

    other_boreholes = [bh for bh in BOREHOLES_SET2 ]

    for bh in other_boreholes:
        sec = get_sections(client,bh,start,end)
        fig = plotting_hydraulics(sec,title=bh)
        figures.append(fig)


    return figures

def create_graph_element(id):
    return dcc.Graph(id=id,style={'width': '100%','height': '70vh'})

def render():

    return html.Div([
        dmc.Title('Hydraulic data Visualisation - VALTER'),
        dmc.Container(
            dmc.Group(
                spacing=50,
                align='end',
                mt=20,
                children=[
                    dmc.DatePicker(
                        id='date-start',
                        label='Start Date',
                        value=(datetime.now(timezone.utc) - timedelta(hours=12))
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='Start Time',
                        id='time-start',
                        value=(datetime.now(timezone.utc) - timedelta(hours=12))
                        .replace(tzinfo=None, microsecond=0)),
                    dmc.DatePicker(
                        id='date-end',
                        label='End Date',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='End Time', id='time-end',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0)),
                    dmc.Button(
                        id='submit-button-state',
                        n_clicks=0,
                        children='Submit'),
                ],
            )),
        dcc.Loading(
            id='loading-icon',
            children=[create_graph_element('graphVALTER'+str(idx)) for idx in range(len(BOREHOLES_SET2))],
            type='default',
            style={'marginTop': '50px', 'alignSelf': 'flex-start'})
        ])


# Layout
layout = render
