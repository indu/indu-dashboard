from datetime import datetime, timedelta, timezone

import dash
import dash_mantine_components as dmc
from dash import Input, Output, State, callback, dcc, html
from hydws.client import HYDWSDataSource

from app.functions.global_variables import HYDWS_URL,INJ_BOREHOLE,INJ_SECTION,BOREHOLES_SET1
from app.functions.plotting_functions import plotting_hydraulics
from app.functions.fetch_hydraulics import get_sections


BOREHOLES_SET1=list(BOREHOLES_SET1.keys())
if INJ_BOREHOLE in BOREHOLES_SET1:
    BOREHOLES_SET1.remove(INJ_BOREHOLE)
    BOREHOLES_SET1.insert(0, INJ_BOREHOLE)

dash.register_page(__name__, name='HYDWS (testing)', title='HYDWS (testing)')

url_hydraulics = f'{HYDWS_URL}/hydws/v1'

client = HYDWSDataSource(url_hydraulics)

list_out_callback = [Output('graph'+str(idx),'figure') for idx in range(len(BOREHOLES_SET1))]

@callback(list_out_callback,
          Input('submit-button-state', 'n_clicks'),
          State('date-start', 'value'),
          State('time-start', 'value'),
          State('date-end', 'value'),
          State('time-end', 'value'))
def update_graph(n_clicks,
                 date_start,
                 time_start,
                 date_end,
                 time_end):

    time_start = datetime.strptime(time_start, '%Y-%m-%dT%H:%M:%S')
    time_end = datetime.strptime(time_end, '%Y-%m-%dT%H:%M:%S')
    date_start = datetime.strptime(date_start, '%Y-%m-%d')
    date_end = datetime.strptime(date_end, '%Y-%m-%d')

    start = datetime.combine(date_start.date(), time_start.time())
    end = datetime.combine(date_end.date(), time_end.time())

    #section_hydraulics = get_sections(client,INJ_BOREHOLE,start,end)

    #inj_section = [sec for sec in section_hydraulics if sec.metadata["name"]==INJ_SECTION]

    figures=[]

    # Inection point
    #fig1 = plotting_hydraulics(inj_section,title=INJ_SECTION, injection_point=True)
    #figures.append(fig1)

    # ST1
    #fig2 = plotting_hydraulics(section_hydraulics,title=INJ_BOREHOLE)
    #figures.append(fig2)

    #other_boreholes = [bh for bh in BOREHOLES_SET1 if bh!=INJ_BOREHOLE]

    for bh in BOREHOLES_SET1:
        try:
            sec = get_sections(client,bh,start,end)
        except:
            sec = None
        fig = plotting_hydraulics(sec,title=bh)
        figures.append(fig)


    return figures

def create_graph_element(id):
    return dcc.Graph(id=id,style={'width': '100%','height': '70vh'})

def render():

    return html.Div([
        dmc.Title('Hydraulic data Visualisation - FEAR'),
        dmc.Container(
            dmc.Group(
                spacing=50,
                align='end',
                mt=20,
                children=[
                    dmc.DatePicker(
                        id='date-start',
                        label='Start Date',
                        value=(datetime.now(timezone.utc) - timedelta(hours=12))
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='Start Time',
                        id='time-start',
                        value=(datetime.now(timezone.utc) - timedelta(hours=12))
                        .replace(tzinfo=None, microsecond=0)),
                    dmc.DatePicker(
                        id='date-end',
                        label='End Date',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='End Time', id='time-end',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0)),
                    dmc.Button(
                        id='submit-button-state',
                        n_clicks=0,
                        children='Submit'),
                ],
            )),
        dcc.Loading(
            id='loading-icon',
            children=[create_graph_element('graph'+str(idx)) for idx in range(len(BOREHOLES_SET1))],
            type='default',
            style={'marginTop': '50px', 'alignSelf': 'flex-start'})
        ])


# Layout
layout = render
