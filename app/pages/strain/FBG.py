from datetime import datetime, timedelta, timezone

import dash
import dash_mantine_components as dmc
import pandas as pd
from dash import Input, Output, State, callback, dcc, html, register_page
from influxdb_client import InfluxDBClient
from obspy import UTCDateTime
from obspy.clients.fdsn import Client


from app.functions.global_variables import (INFLUXDB_URL,INFLUXDB_TOKEN,FDSN_FBG_URL,FDSN_FBG_USER,
                                            FDSN_FBG_PASSWD,FBG_BOREHOLES_DEPTHS,BOREHOLES_SET1)
from app.functions.plotting_functions import plotting_FBG_timeseries,plotting_FBG_waterfall
from app.functions.fetch_strain_data import get_fbg
from app.functions.fetch_hydraulics import get_sections_INFLUXDB

register_page(__name__, name='FBG', title='FBG')

DBclient = InfluxDBClient(url=INFLUXDB_URL, token=INFLUXDB_TOKEN, org="org")
#query_api = DBclient.query_api()

list_out_callback = [Output('FBGgraph'+str(idx),'figure') for idx in range(2)]

@callback(list_out_callback,
          Input('borehole_select','value'),
          Input('radio-borehole-pressure','value'),
          Input('submit-button-state', 'n_clicks'),
          State('date-start', 'value'),
          State('time-start', 'value'),
          State('date-end', 'value'),
          State('time-end', 'value'))
def update_graph(borehole,pp,n_clicks,
                 date_start,
                 time_start,
                 date_end,
                 time_end):

    time_start = datetime.strptime(time_start, '%Y-%m-%dT%H:%M:%S')
    time_end = datetime.strptime(time_end, '%Y-%m-%dT%H:%M:%S')
    date_start = datetime.strptime(date_start, '%Y-%m-%d')
    date_end = datetime.strptime(date_end, '%Y-%m-%d')

    start = datetime.combine(date_start.date(), time_start.time())
    end = datetime.combine(date_end.date(), time_end.time())

    obspycli = Client(base_url=FDSN_FBG_URL, user=FDSN_FBG_USER, password=FDSN_FBG_PASSWD, debug=False)

    stations = obspycli.get_stations(
        network='8R',
        location='*',
        channel='FS1',
        starttime=UTCDateTime.now()-600,
        endtime=UTCDateTime.now()-300
    )

    station_codes = [station.code for station in stations[0]]
    #station_list = [code for code in station_codes if code != 'P12Z1']
    station_list = [code for code in station_codes]
    if borehole == "BFE18":
        station_list = [code for code in station_list if code.startswith('F')]
        factor = 1
    elif borehole == "BPR12":
        station_list = [code for code in station_list if code.startswith('P')]
        factor = 0.1


    dataframes = get_fbg(DBclient,'tests_here',station_list,start,end)

    #station_list = [list(BOREHOLES_SET1[borehole]['channels'].keys()) for borehole in BOREHOLES_SET1]
    #bucket_list = [BOREHOLES_SET1[borehole]['bucket'] for borehole in BOREHOLES_SET1]
    #scaling_list = [[BOREHOLES_SET1[borehole]['channels'][channel]['scaling']
    #                 for channel in BOREHOLES_SET1[borehole]['channels']]
    #                for borehole in BOREHOLES_SET1
    #]

    #station_list = [list(BOREHOLES_SET1[borehole]['channels'].keys()) for borehole in BOREHOLES_SET1]

    if pp == 'None':
        df_hydr=pd.DataFrame()
    else:
        channel_list = list(BOREHOLES_SET1[pp]['channels'].keys())
        scaling_list = [BOREHOLES_SET1[pp]['channels'][channel]['scaling']
                         for channel in BOREHOLES_SET1[pp]['channels']]
        df_hydr = get_sections_INFLUXDB(DBclient,'tests_here',channel_list,scaling_list,start,end)
        columns_hy = [key[0] for key in BOREHOLES_SET1[pp]['channels'].items()]
        df_hydr = df_hydr[columns_hy]
        maps_hydr = {key: value['description'] for key, value in BOREHOLES_SET1[pp]['channels'].items()}
        df_hydr.rename(columns=maps_hydr, inplace=True)


    figures=[]

    fig1 = plotting_FBG_timeseries(dataframes,FBG_BOREHOLES_DEPTHS[borehole],df_hydr)
    figures.append(fig1)


    fig2 = plotting_FBG_waterfall(dataframes,FBG_BOREHOLES_DEPTHS[borehole],factor)
    figures.append(fig2)

    # ST1
    #fig2 = plotting_hydraulics(section_hydraulics,title=INJ_BOREHOLE)
    #figures.append(fig2)

    #other_boreholes = [bh for bh in BOREHOLES_SET1 if bh!=INJ_BOREHOLE]

    #for bh in other_boreholes:
    #    try:
    #        sec = get_sections(client,bh,start,end)
    #    except:
    #        sec = None
    #    fig = plotting_hydraulics(sec,title=bh)
    #    figures.append(fig)


    return figures

def create_graph_element(id):
    return dcc.Graph(id=id,style={'width': '100%','height': '70vh'})

def render():

    return html.Div([
    dmc.Title('FBG Visualization'),
    html.Br(),
    html.Table(style={'width': '99%', 'cellPadding': '1',
                      'cellSpacing': '1', 'border': '0'}, children=[
        html.Tr(children=[
            html.Td(children=[
                dmc.Select(
                    id='borehole_select',
                    data=[
                        {'label': 'FEAR - BFE18', 'value': "BFE18"},
                        {'label': 'Precode - BPR12', 'value': "BPR12"},
                    ],
                    value="BFE18",
                    clearable=False,
                ),
            ])
        ]),
        dmc.Container(
            dmc.Group(
                spacing=50,
                align='end',
                mt=20,
                children=[
                    dmc.DatePicker(
                        id='date-start',
                        label='Start Date',
                        value=(datetime.now(timezone.utc) - timedelta(hours=12))
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='Start Time',
                        id='time-start',
                        value=(datetime.now(timezone.utc) - timedelta(hours=12))
                        .replace(tzinfo=None, microsecond=0)),
                    dmc.DatePicker(
                        id='date-end',
                        label='End Date',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='End Time', id='time-end',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0)),
                    dmc.Button(
                        id='submit-button-state',
                        n_clicks=0,
                        children='Submit'),
                ],
            )),
        #dcc.RadioItems(options=['BFE05', 'BFE06', 'None'],
        #               value='None',
        #               inline=True,
        #               style={'marginTop': '50px', 'alignSelf': 'flex-start'},),
        ]),
        dmc.RadioGroup(
            [dmc.Radio(l, value=k) for k, l in [["BFE05", 'BFE05'], ['BFE06', 'BFE06'], ['None', 'None']]],
            id="radio-borehole-pressure",
            value='None',
            label="Select borehole pressure",
            size="sm",
            mt=20,
            style={
                'marginTop': '20px',
                'alignSelf': 'flex-start',
                'textAlign': 'left',
                'alignItems': 'flex-start'
            }
        ),
        dcc.Loading(
            id='loading-icon',
            children=[create_graph_element('FBGgraph' + str(idx)) for idx in range(2)],
            type='default',
            style={'marginTop': '50px', 'alignSelf': 'flex-start'})
    ])


# Layout
layout = render
