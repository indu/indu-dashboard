import os
import numpy as np

import dash
import dash_mantine_components as dmc
import plotly.graph_objs as go
from plotly.subplots import make_subplots
from dash import Input, Output, callback, dcc, html, State

from app.functions.fetch_catalogue import load_events, load_events_ges
from app.functions.global_variables import MAGNITUDE_COLUMN, TIME_COLUMN
from app.functions.plotting_functions import (build_borehole_traces,
                                              load_boreholes, mag_to_size)
from app.functions.figure_styling import basic_style_2yaxes

#dash.register_page(__name__)

assets_dir = os.path.abspath('app/assets')

@callback(
    Output('plotly3Dscatter2', 'children'),
    Input('dataSelect', 'value'),
    Input('minPhases', 'value'),
    Input('plotly_scatter_time2','relayoutData'),
)
def update_data_and_plots(selected_catalog, min_phases,relayoutData):

    if selected_catalog == os.path.join(
            assets_dir, 'catalogs', 'ges.csv'):
        events = load_events_ges(selected_catalog)
    else:
        events = load_events(selected_catalog, 0.01, min_phases)

    events['hover_text'] = (
        'Time: ' + events[TIME_COLUMN].astype(str) + '<br>' +
        'Magnitude: ' + events[MAGNITUDE_COLUMN].astype(str)
    )

    x_range = [0,1]

    if relayoutData and 'xaxis.range[0]' in relayoutData:
        x_range[0] = np.datetime64(relayoutData['xaxis.range[0]'])
        x_range[1] = np.datetime64(relayoutData['xaxis.range[1]'])
        events = events[(events[TIME_COLUMN].dt.tz_localize(None) >= x_range[0])
                        & (events[TIME_COLUMN].dt.tz_localize(None) <= x_range[1])]
    else:
        events= events

    events_rate = events.copy()
    events_rate["rate"] = np.ones(len(events_rate))
    events_rate = events_rate.resample('1min', on=TIME_COLUMN).sum()

    scatter_time_fig = make_subplots(specs=[[{'secondary_y': True}]])

    scatter_time_fig.add_trace(go.Scatter(
        x=events[TIME_COLUMN],
        y=np.arange(1, len(events) + 1),
        mode='lines+markers',
        line_color='#000000',
        name="cumulative"), secondary_y=False)

    scatter_time_fig.add_trace(go.Scatter(
        x=events_rate.index,
        y=events_rate['rate'],
        mode='lines',
        line_shape = 'hv',
        fill='tozeroy',
        line_color='#D57272',
        name = 'events/minute'), secondary_y=True)

    basic_style_2yaxes(scatter_time_fig)

    scatter_time_fig.update_layout(xaxis=dict(title='Time',showgrid=False),
                                   yaxis=dict(title='Number of events',showgrid=False),
                                   yaxis2=dict(title='Rate of events',showgrid=False),
                                   autosize=True,
                                   height=300,
                                   font=dict(
                                       family='Verdana',
                                       size=14,
                                   ),
                                   plot_bgcolor='white')

    scatter3d_fig = plot_3d_scatter(events)
    camera = dict(
        up=dict(x=0, y=0, z=1),
        center=dict(x=0, y=0, z=0),
        eye=dict(x=-1, y=-2, z=1.25)
    )
    scatter3d_fig.update_layout(autosize=True,
                                height=1200,
                                font=dict(
                                    family='Verdana',
                                    size=14,
                                ),
                                scene={'aspectmode': 'cube'},
                                scene_camera=camera)
    scatter3d_fig.update_scenes(xaxis_title_text='Easting [m]',
                                yaxis_title_text='Northing [m]',
                                zaxis_title_text='Altitude [m]',)

    return dcc.Graph(id='plotly_scatter_time2',figure=scatter_time_fig),dcc.Graph(figure=scatter3d_fig)

def plot_3d_scatter(events):
    # Plot 3D scatter
    scatter3d_fig = go.Figure()
    # Add traces for events
    scatter3d_fig.add_trace(build_event_trace(events))
    bh = load_boreholes(os.path.join(
        assets_dir, 'data_bedretto', 'coord_boreholes_tunnel.json'))
    traces = build_borehole_traces(bh)
    for trace in traces:
        scatter3d_fig.add_trace(trace)
    # Add other traces if needed
    return scatter3d_fig


def build_event_trace(events):
    # Build event trace
    event_trace = go.Scatter3d(
        x=events['x'],
        y=events['y'],
        z=events['z'],
        mode='markers',
        marker=dict(
            size=events[MAGNITUDE_COLUMN].apply(
                lambda x: mag_to_size(30, x)),
            color='rgb(17, 157, 255)',
            opacity=0.5,
            symbol='circle',
            line=dict(color='rgb(17, 157, 255)', width=0.1)
        ),
        name='events',
        hoverinfo='text',
        text=events['hover_text']
    )
    return event_trace

def render():

    return html.Div([
        dmc.Title('Spatial distribution of recorded seismicity'),
        html.Br(),
        html.Table(style={'width': '99%', 'cellPadding': '1',
                          'cellSpacing': '1', 'border': '0'}, children=[
            html.Tr(children=[
                html.Td(children=[
                    dmc.Select(
                        id='dataSelect',
                        data=[
                            {'label': 'Catalog: experimental catalog', 'value': os.path.join(
                                assets_dir, 'catalogs', 'seiscomp.csv')},
                            {'label': 'Catalog: DD catalog',
                             'value': os.path.join(assets_dir, 'catalogs',
                                                   'seiscomp-dd.csv')},
                            #{'label': 'malmi_2024.csv',
                            # 'value': os.path.join(assets_dir, 'catalogs',
                            #                       'malmi_2024.csv')},
                            #{'label': 'ges.csv', 'value': os.path.join(
                            #    assets_dir, 'catalogs', 'ges.csv')}
                        ],
                        value=os.path.join(
                            assets_dir, 'catalogs', 'seiscomp.csv'),
                        clearable=False,
                    ),
                ])
            ]),
            html.Tr(children=[
                html.Td(children=[
                    dmc.Select(
                        id='minPhases',
                        data=[
                            {'label': f'Number of phases: Min {i} phases', 'value': i}
                            for i in range(0, 17, 2)
                        ],
                        value=8,
                        clearable=False,
                    ),
                ])
            ]),

            ]),
        dcc.Loading(
            id='loading-iconSH',
            children=[html.Div(id='plotly_scatter_time2')],
            type='default',
            style={'width': '100%'}
        ),
        dcc.Loading(
            id='loading-iconSH_time',
            children=[html.Div(id='plotly3Dscatter2')],
            type='default',
            style={'width': '100%'}
        ),
    ]),

layout = render