import os
from datetime import datetime, timedelta, timezone
import numpy as np

import dash
import dash_mantine_components as dmc
import plotly.graph_objs as go
from dash import Input, Output, callback, dcc, html, State, callback_context

from app.functions.fetch_catalogue import load_events, load_events_ges
from app.functions.data_processing import spatial_bvalue
from app.functions.global_variables import MAGNITUDE_COLUMN, TIME_COLUMN, BINNING
from app.functions.plotting_functions import (plot_b_value_space,build_borehole_traces,
                                              load_boreholes, mag_to_size)


dash.register_page(__name__, name='Spatial b-value', title='Spatial b-value')

assets_dir = os.path.abspath('app/assets')

@callback(
    [Output('date-startS2', 'value'),
    Output('date-endS2', 'value')],
    Input('dataSelect2', 'value')
)
def update_start_date(catalog):
    options = {
        os.path.join(assets_dir, 'catalogs', 'seiscomp.csv'): datetime(2024, 11, 25),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-dd.csv'): datetime(2024, 11, 25),
        os.path.join(assets_dir, 'catalogs', 'seiscompM0B.csv'): datetime(2024, 8, 20),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0B.csv'): datetime(2024, 8, 20),
        os.path.join(assets_dir, 'catalogs', 'seiscompM0A.csv'): datetime(2024, 4, 15),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0A.csv'): datetime(2024, 4, 15),
    }

    options_end = {
        os.path.join(assets_dir, 'catalogs', 'seiscomp.csv'): datetime.now(),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-dd.csv'): datetime.now(),
        os.path.join(assets_dir, 'catalogs', 'seiscompM0B.csv'): datetime(2024, 9, 10),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0B.csv'): datetime(2024, 9, 10),
        os.path.join(assets_dir, 'catalogs', 'seiscompM0A.csv'): datetime(2024, 5, 10),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0A.csv'): datetime(2024, 5, 10),
    }
    return [options.get(catalog, datetime(2024, 1, 1)).replace(tzinfo=None, microsecond=0).strftime('%Y-%m-%d'),
            options_end.get(catalog, datetime(2024, 1, 1)).replace(tzinfo=None, microsecond=0).strftime('%Y-%m-%d')]


@callback(
    Output('plotly_b_value_scatter', 'children'),
    Input('dataSelect2', 'value'),
    Input('minPhases2', 'value'),
    Input('scaler2', 'value'),
    Input('submit-button-stateS2', 'n_clicks'),
    State('date-startS2', 'value'),
    State('time-startS2', 'value'),
    State('date-endS2', 'value'),
    State('time-endS2', 'value')
)
def update_data_and_plots(selected_catalog, min_phases,scaler,
                          n_clicks,
                 date_start,
                 time_start,
                 date_end,
                 time_end):

    # Fetch the latest value of all inputs
    ctx = callback_context
    triggered_inputs = {trigger["prop_id"].split(".")[0]: trigger["value"] for trigger in ctx.triggered}

    # Ensure start date and time are up-to-date
    if 'dataSelect2' in triggered_inputs:
        # This ensures the start date and time are updated based on the catalog
        TS,TE = update_start_date(selected_catalog)
        date_start = datetime.strptime(TS,'%Y-%m-%d')
        time_start = datetime.strptime(TS,'%Y-%m-%d')
        date_end = datetime.strptime(TE,'%Y-%m-%d')
        time_end = datetime.strptime(TE,'%Y-%m-%d')
    else:
        time_start = datetime.strptime(time_start, '%Y-%m-%dT%H:%M:%S')
        date_start = datetime.strptime(date_start, '%Y-%m-%d')
        time_end = datetime.strptime(time_end, '%Y-%m-%dT%H:%M:%S')
        date_end = datetime.strptime(date_end, '%Y-%m-%d')

    start = datetime.combine(date_start.date(), time_start.time())
    end = datetime.combine(date_end.date(), time_end.time())

    if selected_catalog == os.path.join(
            assets_dir, 'catalogs', 'ges.csv'):
        events = load_events_ges(selected_catalog)
    else:
        #events = load_events_from_csv(selected_catalog)
        events = load_events(selected_catalog, BINNING, min_phases)

    events=events[events[TIME_COLUMN].dt.tz_localize(None)>=np.datetime64(start)]
    events=events[events[TIME_COLUMN].dt.tz_localize(None)<=np.datetime64(end)]

    #events = events.sort_values(by=TIME_COLUMN)
    if len(events)>0:
        events['seconds']=((events[TIME_COLUMN].values
                            - min(events[TIME_COLUMN].values))
                           /np.timedelta64(1, 's'))

        events['hover_text'] = (
            'Time: ' + events[TIME_COLUMN].astype(str) + '<br>' +
            'Magnitude: ' + events[MAGNITUDE_COLUMN].astype(str)
        )

        b_value_space = spatial_bvalue(events, 300)
    else:
        b_value_space={'b-value':None}


    cat_valter = [os.path.join(assets_dir, 'catalogs', 'seiscompM0A.csv'),
                  os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0A.csv'),
                  os.path.join(assets_dir, 'catalogs', 'seiscompM0B.csv'),
                  os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0B.csv')]

    region = "VALTER"  if selected_catalog in cat_valter else "FEAR"

    scatter3d_fig = plot_b_value_space(events, scaler, b_value_space, 'b-value')
    if region == "VALTER":
        bh = load_boreholes(os.path.join(
            assets_dir, 'data_bedretto', 'coord_boreholes_tunnel_VALTER.json'))
    elif region == "FEAR":
        bh = load_boreholes(os.path.join(
            assets_dir, 'data_bedretto', 'coord_boreholes_tunnel_FEAR.json'))
    traces = build_borehole_traces(bh)
    for trace in traces:
        scatter3d_fig.add_trace(trace)

    camera = dict(
        up=dict(x=0, y=0, z=1),
        center=dict(x=0, y=0, z=0),
        eye=dict(x=-1, y=-2, z=1.25)
    )
    scatter3d_fig.update_layout(autosize=True,
                                height=1200,
                                font=dict(
                                    family='Verdana',
                                    size=14,
                                ),
                                scene={'aspectmode': 'cube'},
                                scene_camera=camera,
                                )

    scatter3d_fig.update_scenes(xaxis_title_text='Easting [m]',
                                yaxis_title_text='Northing [m]',
                                zaxis_title_text='Altitude [m]',)
    return dcc.Graph(figure=scatter3d_fig)

def render():

    return html.Div([
    dmc.Title('Spatial distribution of b-value'),
    html.Br(),
    html.Table(style={'width': '99%', 'cellPadding': '1',
                      'cellSpacing': '1', 'border': '0'}, children=[
        html.Tr(children=[
            html.Td(children=[
                dmc.Select(
                    id='dataSelect2',
                    data=[
                        {'label': 'Catalog: experimental catalog FEAR1', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscomp.csv')},
                        {'label': 'Catalog: DD catalog FEAR1', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscomp-dd.csv')},
                        {'label': 'Catalog: experimental catalog MZeroB', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscompM0B.csv')},
                        {'label': 'Catalog: DD catalog MZeroB', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscomp-ddM0B.csv')},
                        {'label': 'Catalog: experimental catalog MZeroA', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscompM0A.csv')},
                        {'label': 'Catalog: DD catalog MZeroA', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscomp-ddM0A.csv')}
                    ],
                    value=os.path.join(
                        assets_dir, 'catalogs', 'seiscomp.csv'),
                    clearable=False,
                ),
            ])
        ]),
        html.Tr(children=[
            html.Td(children=[
                dmc.Select(
                    id='minPhases2',
                    data=[
                        {'label': f'Number of phases: Min {i} phases', 'value': i}
                        for i in range(0, 17, 2)
                    ],
                    value=8,
                    clearable=False,
                ),
            ])
        ]),
        html.Tr(children=[
            html.Td(children=[
                html.Br(),
                dmc.Text('Magnitude Scaler:',size="sm"),
                dmc.NumberInput(
                    id='scaler2',
                    value=50,
                    min = 1,
                    step = 5,
                ),
            ],style={'display': 'inline-block', 'textAlign': 'left'})
        ],style={'textAlign': 'left'}),
        dmc.Container(
            dmc.Group(
                spacing=50,
                align='end',
                mt=20,
                children=[
                    dmc.DatePicker(
                        id='date-startS2',
                        label='Start Date',
                        value=(datetime(2024,11,25))
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='Start Time',
                        id='time-startS2',
                        value=(datetime(2024,11,25))
                        .replace(tzinfo=None, microsecond=0)),
                    dmc.DatePicker(
                        id='date-endS2',
                        label='End Date',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='End Time', id='time-endS2',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0)),
                    dmc.Button(
                        id='submit-button-stateS2',
                        n_clicks=0,
                        children='Submit'),
                ],
            )),
        dcc.Loading(
            id='loading-iconSH',
            children=[html.Div(id='plotly_b_value_scatter')],
            type='default',
            style={'marginTop': '50px', 'alignSelf': 'flex-start'})
    ])
])

layout = render


# def render():
#     binning = BINNING
#
#     # Load catalogues
#     seiscomp_path = os.path.join(assets_dir, 'catalogs', 'seiscomp.csv')
#     seiscomp_catalogue = load_events(seiscomp_path, binning, MIN_NUM_PHASES)
#
#     Nevents = 100
#     overlap_window = 0.1
#     # Hardcoded time interval, could be fetched from forecast series if desired
#     time_interval_in_minutes = 10
#
#     b_value_space = spatial_bvalue(seiscomp_catalogue,300)
#
#     fig3 = plot_b_value_space(seiscomp_catalogue,b_value_space,'b-value')
#     bh = load_boreholes(os.path.join(
#         assets_dir, 'data_bedretto', 'coord_boreholes_tunnel.json'))
#     traces = build_borehole_traces(bh)
#     for trace in traces:
#         fig3.add_trace(trace)
#
#     camera = dict(
#         up=dict(x=0, y=0, z=1),
#         center=dict(x=0, y=0, z=0),
#         eye=dict(x=-1, y=-2, z=1.25)
#     )
#     fig3.update_layout(autosize=True,
#                        height=800,
#                        font=dict(
#                              family='Verdana',
#                              size=14,
#                        ),
#                        scene={'aspectmode': 'cube'},
#                        scene_camera=camera,
#                        )
#
#     return dcc.Graph(figure=fig3)
#

# Tab layout
# layout = html.Div([
#         dmc.Container([
#             dmc.Title('b-value in space'),
#             html.H3(
#                 f'Mc calculated with maximum curvature method \
#                   (+0.2 correction factor); \
#                   b-value calculated with MLE with correction for binning; \
#                   sampling size: 300 events'),
#             html.H3('Minimum number of phases used: ' + str(MIN_NUM_PHASES)),
#         ]),
#         render()
#
#     ], style={'width': '100%'})
