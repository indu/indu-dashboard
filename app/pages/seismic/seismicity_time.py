import os

import dash
import dash_mantine_components as dmc
import numpy as np
import plotly.graph_objects as go
from dash import Input, Output, callback, dcc, html
from datetime import datetime

from seismostats.io.client import FDSNWSEventClient as FDSNclient

from app.functions.fetch_catalogue import load_events,load_events_ges, load_events_fdsn
from app.functions.figure_styling import TLS_COLORS
from app.functions.global_variables import (BINNING, MAG_ORANGE_TLS,
                                            MAG_RED_TLS, MAGNITUDE_COLUMN,
                                            TIME_COLUMN, FDSN_URL)
from app.functions.plotting_functions import mag_to_size

TLS_orange_level = MAG_ORANGE_TLS
TLS_red_level = MAG_RED_TLS


dash.register_page(__name__)

assets_dir = os.path.abspath('app/assets')

@callback(
    Output('plotlyscatterST', 'children'),
    Input('dataSelectST', 'value'),
    Input('minPhasesST', 'value'),
    Input('rateST', 'value')
)
def update_data_and_plots(selected_catalog,min_phases,rate):
    '''
    Update the scatter plot of the seismicity in time (with selected catalogue)
    '''
    if selected_catalog == os.path.join(assets_dir, 'catalogs', 'ges.csv'):
        events = load_events_ges(selected_catalog)
    elif selected_catalog == os.path.join(assets_dir, 'catalogs', 'seiscomp-bg.csv'):
        events = load_events_fdsn(FDSNclient(FDSN_URL+'/fdsnws/event/1/query'), datetime(2024,11,11,10), datetime.now(), BINNING, number_phases=min_phases)
    elif selected_catalog == os.path.join(assets_dir, 'catalogs', 'seiscomp-bgM0B.csv'):
        events = load_events_fdsn(FDSNclient(FDSN_URL+'/fdsnws/event/1/query'), datetime(2024,8,26,10), datetime.now(), BINNING, number_phases=min_phases)
    elif selected_catalog == os.path.join(assets_dir, 'catalogs', 'seiscomp-bgM0A.csv'):
        events = load_events_fdsn(FDSNclient(FDSN_URL+'/fdsnws/event/1/query'), datetime(2024,4,15,10),  datetime(2024,5,3,10), BINNING, number_phases=min_phases)
    else:
        events = load_events(selected_catalog, BINNING, min_phases)

    events["cumulative"]=np.arange(1, len(events) + 1)
    events_rate = events.copy()
    events_rate["rate"] = np.ones(len(events_rate))
    events_rate = events_rate.resample(str(rate/60)+'min',on=TIME_COLUMN).sum()

    timeline_fig = go.Figure()
    timeline_fig.add_trace(go.Scatter(x=events[TIME_COLUMN],
                                      y=events[MAGNITUDE_COLUMN],
                                      mode='markers',
                                      marker=dict(size=events[MAGNITUDE_COLUMN].apply(
                                          lambda x: mag_to_size(30, x)),
                                          color='rgb(17, 157, 255)',
                                          opacity=0.8,
                                          symbol='circle',
                                          line=dict(color='rgb(17, 157, 255)',width=0.1)),
                                      name='Magnitude of the events'))

    timeline_fig.update_traces(marker=dict(color=np.where(
        events[MAGNITUDE_COLUMN] < TLS_red_level,
        TLS_COLORS[1], TLS_COLORS[2])))
    timeline_fig.update_traces(marker=dict(color=np.where(
        events[MAGNITUDE_COLUMN] < TLS_orange_level, TLS_COLORS[0],
        timeline_fig.data[0].marker.color)))

    timeline_fig.add_trace(go.Scatter
                           (x=events[TIME_COLUMN], y=np.arange(1, len(
                               events) + 1), mode='lines',
                               name='Cumulative number of events',yaxis = "y2"))
    timeline_fig.update_traces(line_color='#000000', selector=dict(
        name='Cumulative number of events'))

    match rate:
        case 60:
            title_rate = 'Rate of events per minute'
        case 900:
            title_rate = 'Rate of events in 15 minutes'
        case 3600:
            title_rate = 'Rate of events per hour'


    timeline_fig.add_trace(go.Scatter
                           (x=events_rate.index, y=events_rate['rate'], mode='lines', line_shape = 'hv',
                               name=title_rate,yaxis="y3", fill='tozeroy'))

    timeline_fig.update_traces(line_color='#D57272', selector=dict(
        name=title_rate))




    timeline_fig.update_layout(
        xaxis=dict(
            domain=[0, 0.8]
        ),
        yaxis=dict(title='Magnitude'),
        yaxis2=dict(title='Cumulative number of events',
                    anchor="x",
                    overlaying="y",
                    side="right"),
        yaxis3=dict(title=title_rate,
                    titlefont=dict(color="#d62728"),
                    tickfont=dict(color="#d62728"),
                    anchor="free",
                    overlaying="y",
                    side="right",
                    position = 0.95),
    )


    timeline_fig.update_layout(autosize=True,
                               height=800,
                               font=dict(
                                   family='Verdana',
                                   size=16,
                              ), plot_bgcolor='white')

    timeline_fig.update_xaxes(
        ticks='outside',
        showline=True,
        linecolor='black',
        gridcolor='lightgrey')

    timeline_fig.update_yaxes(
        ticks='outside',
        showline=True,
        linecolor='black',
        gridcolor='lightgrey',
        showgrid=False)

    return dcc.Graph(figure=timeline_fig)

def render():

    return html.Div([
        dmc.Title('Temporal evolution of the seismicity'),
        # html.H3('/!\ Disclaimer: Seiscomp magnitudes are \
        #     MLc, GES & Malmi magnitudes are Mw'),

        html.Br(),
        html.Table(style={'width': '99%', 'cellPadding': '1',
                          'cellSpacing': '1', 'border': '0'},
                   children=[
                       html.Tr(children=[
                           html.Td(children=[
                               dmc.Select(id='dataSelectST',
                                          data=[
                                              {'label': 'Catalog: background catalog', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscomp-bg.csv')},
                                              {'label': 'Catalog: experimental catalog FEAR1', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscomp.csv')},
                                              {'label': 'Catalog: DD catalog FEAR1', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscomp-dd.csv')},
                                              {'label': 'Catalog: experimental catalog MZeroB', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscompM0B.csv')},
                                              {'label': 'Catalog: DD catalog MZeroB', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscomp-ddM0B.csv')},
                                              {'label': 'Catalog: experimental catalog MZeroA', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscompM0A.csv')},
                                              {'label': 'Catalog: DD catalog MZeroA', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscomp-ddM0A.csv')}
                                          ],
                                          value=os.path.join(assets_dir, 'catalogs', 'seiscomp.csv'),
                                          clearable=False,
                                          ), ]
                           ), ]
                       ),
                       html.Tr(children=[
                           html.Td(children=[
                               dmc.Select(
                                   id='minPhasesST',
                                   data=[
                                       {'label': f'Number of phases: Min {i} phases', 'value': i}
                                       for i in range(0, 17, 2)
                                   ],
                                   value=8,
                                   clearable=False,
                               ),
                           ]),
                       ]),
                       html.Tr(children=[
                           html.Td(children=[
                               dmc.Select(
                                   id='rateST',
                                   data=[
                                       {'label': 'Rate of seismicity: 1 minute', 'value': 60},
                                       {'label': 'Rate of seismicity: 15 minutes', 'value': 900},
                                       {'label': 'Rate of seismicity: 1 hour', 'value': 3600}],
                                   value=60,
                                   clearable=False,
                               ),
                           ]),
                       ]),
                       html.Div(id='plotlyscatterST'),
                   ]),
    ])

layout = render