import os
from datetime import datetime, timedelta, timezone
import numpy as np

import dash
import dash_mantine_components as dmc
import plotly.graph_objs as go
from dash import Input, Output, callback, dcc, html, State, callback_context

from seismostats.io.client import FDSNWSEventClient as FDSNclient
from app.functions.fetch_catalogue import load_events, load_events_ges, load_events_fdsn
from app.functions.global_variables import FDSN_URL_EXP,MAGNITUDE_COLUMN, TIME_COLUMN
from app.functions.plotting_functions import (build_borehole_traces,
                                              load_boreholes, mag_to_size)

dash.register_page(__name__)

assets_dir = os.path.abspath('app/assets')

@callback(
    [Output('date-startS', 'value'),
    Output('date-endS', 'value')],
    Input('dataSelect', 'value')
)
def update_start_date(catalog):
    options = {
        os.path.join(assets_dir, 'catalogs', 'seiscomp.csv'): datetime(2024, 11, 25),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-dd.csv'): datetime(2024, 11, 25),
        os.path.join(assets_dir, 'catalogs', 'seiscompM0B.csv'): datetime(2024, 8, 20),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0B.csv'): datetime(2024, 8, 20),
        os.path.join(assets_dir, 'catalogs', 'seiscompM0A.csv'): datetime(2024, 4, 15),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0A.csv'): datetime(2024, 4, 15),
    }

    options_end = {
        os.path.join(assets_dir, 'catalogs', 'seiscomp.csv'): datetime.now(),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-dd.csv'): datetime.now(),
        os.path.join(assets_dir, 'catalogs', 'seiscompM0B.csv'): datetime(2024, 9, 10),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0B.csv'): datetime(2024, 9, 10),
        os.path.join(assets_dir, 'catalogs', 'seiscompM0A.csv'): datetime(2024, 5, 10),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0A.csv'): datetime(2024, 5, 10),
    }
    return [options.get(catalog, datetime(2024, 1, 1)).replace(tzinfo=None, microsecond=0).strftime('%Y-%m-%d'),
            options_end.get(catalog, datetime(2024, 1, 1)).replace(tzinfo=None, microsecond=0).strftime('%Y-%m-%d')]

@callback(
Output('plotly3Dscatter', 'children'),
[Input('dataSelect', 'value'),
Input('minPhases', 'value'),
Input('scaler', 'value'),
Input('submit-button-stateS', 'n_clicks'),
State('date-startS', 'value'),
State('time-startS', 'value'),
State('date-endS', 'value'),
State('time-endS', 'value')]
)
def update_data_and_plots(selected_catalog, min_phases,scaler,
                          n_clicks,
                 date_start,
                 time_start,
                 date_end,
                 time_end):

    # Fetch the latest value of all inputs
    ctx = callback_context
    triggered_inputs = {trigger["prop_id"].split(".")[0]: trigger["value"] for trigger in ctx.triggered}

    # Ensure start date and time are up-to-date
    if 'dataSelect' in triggered_inputs:
        # This ensures the start date and time are updated based on the catalog
        TS,TE = update_start_date(selected_catalog)
        date_start = datetime.strptime(TS,'%Y-%m-%d')
        time_start = datetime.strptime(TS,'%Y-%m-%d')
        date_end = datetime.strptime(TE,'%Y-%m-%d')
        time_end = datetime.strptime(TE,'%Y-%m-%d')
    else:
        time_start = datetime.strptime(time_start, '%Y-%m-%dT%H:%M:%S')
        date_start = datetime.strptime(date_start, '%Y-%m-%d')
        time_end = datetime.strptime(time_end, '%Y-%m-%dT%H:%M:%S')
        date_end = datetime.strptime(date_end, '%Y-%m-%d')

    start = datetime.combine(date_start.date(), time_start.time())
    end = datetime.combine(date_end.date(), time_end.time())

    if selected_catalog == os.path.join(
            assets_dir, 'catalogs', 'seiscomp.csv'):
        events = load_events(selected_catalog, 0.01, min_phases)
        #events = load_events_fdsn(FDSNclient(FDSN_URL_EXP + '/fdsnws/event/1/query'), start, end, 0.01, min_phases,
        #                           rotation=True)
    else:
        #events = load_events_from_csv(selected_catalog)
        events = load_events(selected_catalog, 0.01, min_phases)


    events=events[events[TIME_COLUMN].dt.tz_localize(None)>=np.datetime64(start)]
    events=events[events[TIME_COLUMN].dt.tz_localize(None)<=np.datetime64(end)]

    #events = events.sort_values(by=TIME_COLUMN)
    if len(events)>0:
        events['seconds']=((events[TIME_COLUMN].values
                            - min(events[TIME_COLUMN].values))
                           /np.timedelta64(1, 's'))

        events['hover_text'] = (
            'Time: ' + events[TIME_COLUMN].astype(str) + '<br>' +
            'Magnitude: ' + events[MAGNITUDE_COLUMN].astype(str)
        )

    cat_valter = [os.path.join(assets_dir, 'catalogs', 'seiscompM0A.csv'),
                  os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0A.csv'),
                  os.path.join(assets_dir, 'catalogs', 'seiscompM0B.csv'),
                  os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0B.csv')]

    region = "VALTER"  if selected_catalog in cat_valter else "FEAR"

    scatter3d_fig = plot_3d_scatter(events,scaler,region)
    camera = dict(
        up=dict(x=0, y=0, z=1),
        center=dict(x=0, y=0, z=0),
        eye=dict(x=-1, y=-2, z=1.25)
    )
    scatter3d_fig.update_layout(autosize=True,
                                height=1200,
                                font=dict(
                                    family='Verdana',
                                    size=14,
                                ),
                                scene={'aspectmode': 'cube'},
                                scene_camera=camera,
                                )

    scatter3d_fig.update_scenes(xaxis_title_text='Easting [m]',
                                yaxis_title_text='Northing [m]',
                                zaxis_title_text='Altitude [m]',)
    return dcc.Graph(figure=scatter3d_fig)


def plot_3d_scatter(events,scaler,region):
    # Plot 3D scatter
    scatter3d_fig = go.Figure()
    # Add traces for events
    scatter3d_fig.add_trace(build_event_trace(events,scaler))
    if region == "VALTER":
        bh = load_boreholes(os.path.join(
            assets_dir, 'data_bedretto', 'coord_boreholes_tunnel_VALTER.json'))
    elif region == "FEAR":
        bh = load_boreholes(os.path.join(
            assets_dir, 'data_bedretto', 'coord_boreholes_tunnel_FEAR.json'))

    traces = build_borehole_traces(bh)
    for trace in traces:
        scatter3d_fig.add_trace(trace)
    # Add other traces if needed
    return scatter3d_fig


def build_event_trace(events,scaler):

    if len(events)>0:
        tick = np.linspace(events['seconds'].values[0],events['seconds'].values[-1],10)
        tick_label = np.array([time[5:] for time in
                               np.datetime_as_string(tick*np.timedelta64(1,'s')+
                               min(events[TIME_COLUMN].values), unit='m')])
    else:
        events['X']=np.nan
        events['Y']=np.nan
        events['Z']=np.nan
        events['seconds']=np.nan
        events['hover_text']= None
        tick = None
        tick_label = None

    # Build event trace
    event_trace = go.Scatter3d(
        x=events['x'],
        y=events['y'],
        z=events['z'],
        mode='markers',
        marker=dict(
            size=events[MAGNITUDE_COLUMN].apply(
                lambda x: mag_to_size(scaler, x)),
            color=events['seconds'],#'rgb(17, 157, 255)',
            opacity=0.5,
            symbol='circle',
            colorbar=dict(
                tickmode = "array",
                tickvals = tick,
                ticktext = tick_label,
                #orientation = "h"
                x=-0.3,
                ticklabelposition = "outside right"
            ),
            colorscale="thermal",
            #line=dict(color='rgb(17, 157, 255)', width=0.1)
        ),
        name='events',
        hoverinfo='text',
        text=events['hover_text'],
    )
    return event_trace

def render():

    return html.Div([
    dmc.Title('Spatial distribution of recorded seismicity'),
    html.Br(),
    html.Table(style={'width': '99%', 'cellPadding': '1',
                      'cellSpacing': '1', 'border': '0'}, children=[
        html.Tr(children=[
            html.Td(children=[
                dmc.Select(
                    id='dataSelect',
                    data=[
                        {'label': 'Catalog: experimental catalog FEAR1', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscomp.csv')},
                        {'label': 'Catalog: DD catalog FEAR1', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscomp-dd.csv')},
                        {'label': 'Catalog: experimental catalog MZeroB', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscompM0B.csv')},
                        {'label': 'Catalog: DD catalog MZeroB', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscomp-ddM0B.csv')},
                        {'label': 'Catalog: experimental catalog MZeroA', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscompM0A.csv')},
                        {'label': 'Catalog: DD catalog MZeroA', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscomp-ddM0A.csv')}
                    ],
                    value=os.path.join(
                        assets_dir, 'catalogs', 'seiscomp.csv'),
                    clearable=False,
                ),
            ])
        ]),
        html.Tr(children=[
            html.Td(children=[
                dmc.Select(
                    id='minPhases',
                    data=[
                        {'label': f'Number of phases: Min {i} phases', 'value': i}
                        for i in range(0, 17, 2)
                    ],
                    value=8,
                    clearable=False,
                ),
            ])
        ]),
        html.Tr(children=[
            html.Td(children=[
                html.Br(),
                dmc.Text('Magnitude Scaler:',size="sm"),
                dmc.NumberInput(
                    id='scaler',
                    value=50,
                    min = 1,
                    step = 5,
                ),
            ],style={'display': 'inline-block', 'textAlign': 'left'})
        ],style={'textAlign': 'left'}),
        dmc.Container(
            dmc.Group(
                spacing=50,
                align='end',
                mt=20,
                children=[
                    dmc.DatePicker(
                        id='date-startS',
                        label='Start Date',
                        value=(datetime(2024,11,25))
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='Start Time',
                        id='time-startS',
                        value=(datetime(2024, 11, 25))
                        .replace(tzinfo=None, microsecond=0),
                        ),
                    dmc.DatePicker(
                        id='date-endS',
                        label='End Date',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='End Time', id='time-endS',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0)
                        ),
                    dmc.Button(
                        id='submit-button-stateS',
                        n_clicks=0,
                        children='Submit'),
                ],
            )),
        dcc.Loading(
            id='loading-iconSH',
            children=[html.Div(id='plotly3Dscatter')],
            type='default',
            style={'marginTop': '50px', 'alignSelf': 'flex-start'})
    ])
])

layout = render