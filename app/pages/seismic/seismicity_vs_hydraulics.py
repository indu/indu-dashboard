import os
from datetime import datetime, timedelta, timezone

import dash
import dash_mantine_components as dmc
import numpy as np
import plotly.graph_objects as go
from dash import Input, Output, callback, dcc, html, State
from plotly.subplots import make_subplots
from influxdb_client import InfluxDBClient
from hydws.client import HYDWSDataSource
from hydws.parser.parser import SectionHydraulics

from seismostats.io.client import FDSNWSEventClient as FDSNclient

from app.functions.global_variables import (HYDWS_URL,INFLUXDB_URL,INFLUXDB_TOKEN,
                                            BOREHOLES_SET1,INJ_BOREHOLE,INJ_SECTION,
                                            BINNING, MAG_ORANGE_TLS,
                                            MAG_RED_TLS, MAGNITUDE_COLUMN,
                                            TIME_COLUMN, FDSN_URL)
from app.functions.fetch_hydraulics import get_sections_INFLUXDB
from app.functions.fetch_catalogue import load_events,load_events_ges,load_events_fdsn

from app.functions.figure_styling import basic_style_2yaxes

TLS_orange_level = MAG_ORANGE_TLS
TLS_red_level = MAG_RED_TLS

dash.register_page(__name__,name='Seismo vs Hydro', title='Seismo vs Hydro')

DBclient = InfluxDBClient(url=INFLUXDB_URL, token=INFLUXDB_TOKEN, org="org")

url_hydraulics = f'{HYDWS_URL}/hydws/v1'
client = HYDWSDataSource(url_hydraulics)

assets_dir = os.path.abspath('app/assets')

@callback(
    Output('plotlyscatterSH', 'children'),
    Input('dataSelectSH', 'value'),
    Input('minPhasesSH', 'value'),
    Input('radio-borehole-pressure3', 'value'),
    Input('dataYL', 'value'),
    Input('dataYR', 'value'),
    Input('submit-button-stateSH', 'n_clicks'),
    State('date-startSH', 'value'),
    State('time-startSH', 'value'),
    State('date-endSH', 'value'),
    State('time-endSH', 'value')
)
def update_data_and_plots(selected_catalog,min_phases,pp,yaxis_left,yaxis_right,
                 n_clicks,
                 date_start,
                 time_start,
                 date_end,
                 time_end):
    '''
    Update the time series plot of the seismicity vs hydraulic with selected catalogue and variables
    '''

    time_start = datetime.strptime(time_start, '%Y-%m-%dT%H:%M:%S')
    time_end = datetime.strptime(time_end, '%Y-%m-%dT%H:%M:%S')
    date_start = datetime.strptime(date_start, '%Y-%m-%d')
    date_end = datetime.strptime(date_end, '%Y-%m-%d')

    start = datetime.combine(date_start.date(), time_start.time())
    end = datetime.combine(date_end.date(), time_end.time())

    if selected_catalog == os.path.join(assets_dir, 'catalogs', 'ges.csv'):
        events = load_events_ges(selected_catalog)
    elif selected_catalog == os.path.join(assets_dir, 'catalogs', 'seiscomp-bg.csv'):
        events = load_events_fdsn(FDSNclient(FDSN_URL+'/fdsnws/event/1/query'), datetime(2024,11,11,10), datetime.now(), BINNING, number_phases=min_phases)
    elif selected_catalog == os.path.join(assets_dir, 'catalogs', 'seiscomp-bgM0B.csv'):
        events = load_events_fdsn(FDSNclient(FDSN_URL+'/fdsnws/event/1/query'), datetime(2024,8,26,10), datetime.now(), BINNING, number_phases=min_phases)
    elif selected_catalog == os.path.join(assets_dir, 'catalogs', 'seiscomp-bgM0A.csv'):
        events = load_events_fdsn(FDSNclient(FDSN_URL+'/fdsnws/event/1/query'), datetime(2024,4,15,10),  datetime(2024,5,3,10), BINNING, number_phases=min_phases)
    else:
        events = load_events(selected_catalog, BINNING, min_phases)

    events=events[events[TIME_COLUMN].dt.tz_localize(None)>=np.datetime64(start)]
    events=events[events[TIME_COLUMN].dt.tz_localize(None)<=np.datetime64(end)]
    events["cumulative"] = np.arange(1, len(events) + 1)

    #inj_section = client.get_section(INJ_BOREHOLE, INJ_SECTION, start, end)
    #df_hydr = SectionHydraulics(inj_section).hydraulics

    channel_list = list(BOREHOLES_SET1[pp]['channels'].keys())
    scaling_list = [BOREHOLES_SET1[pp]['channels'][channel]['scaling']
                     for channel in BOREHOLES_SET1[pp]['channels']]
    df_hydr = get_sections_INFLUXDB(DBclient,'tests_here',channel_list,scaling_list,start,end,60)
    columns_hy = [key[0] for key in BOREHOLES_SET1[pp]['channels'].items()]
    df_hydr = df_hydr[columns_hy]
    maps_hydr = {key: value['description'] for key, value in BOREHOLES_SET1[pp]['channels'].items()}
    df_hydr.rename(columns=maps_hydr, inplace=True)

    df_hydr.rename(columns={"Injection rate":"topflow"}, inplace=True)
    df_hydr.rename(columns={"Injection Pressure":"toppressure"}, inplace=True)
    df_hydr.rename(columns={"Pressure Zone": "bottompressure"}, inplace=True)

    df_hydr["topflow"]=df_hydr["topflow"]/60000
    df_hydr["toppressure"]=df_hydr["toppressure"]*1.e6
    df_hydr["bottompressure"]=df_hydr["bottompressure"]*1.e6

    XL = df_hydr.index
    YL2 = np.array([])

    match yaxis_left:
        case "topflow":
            YL = df_hydr["topflow"].values*60000
            YL_label = "Flow rate (L/min)"
            YL_name = "Injection flow rate"
        case "bottompressure":
            YL = df_hydr["toppressure"].values/1.e6
            YL2 = df_hydr["bottompressure"].values/1.e6
            YL_label = "Pressure (MPa)"
            #YL_name = ["Zone Pressure","Injection Pressure"]
            YL_name = "Injection Pressure"
            YL_name2 = "Zone Pressure"
        case "energy":
            YL = np.nancumsum(df_hydr['topflow'].values*df_hydr["toppressure"].values*60)
            YL_label = "Hydraulic Energy (J)"
            YL_name = "Hydraulic energy"

    YR_line_shape = 'linear'
    YR_fill = 'none'

    match yaxis_right:
        case "cumulative":
            XR = events[TIME_COLUMN]
            YR = events["cumulative"]
            YR_label = "Cumulative number of events"
            YR_name = "Cumulative number of events"
        case "rate":
            events_rate = events.copy()
            events_rate["rate"] = np.ones(len(events_rate))
            events_rate = events_rate.resample('1min', on=TIME_COLUMN).sum()
            XR = events_rate.index
            YR = events_rate["rate"]
            YR_label = "Seismicity rate (events/minute)"
            YR_name = "Seismicity rate"
            YR_line_shape = 'hv'
            YR_fill = 'tozeroy'
        case "rate_hour":
            events_rate = events.copy()
            events_rate["rate"] = np.ones(len(events_rate))
            events_rate = events_rate.resample('60min', on=TIME_COLUMN).sum()
            XR = events_rate.index
            YR = events_rate["rate"]
            YR_label = "Seismicity rate (events/hour)"
            YR_name = "Seismicity rate"
            YR_line_shape = 'hv'
            YR_fill = 'tozeroy'
        case "moment":
            XR = events[TIME_COLUMN]
            #YR = np.cumsum(10**((events[MAGNITUDE_COLUMN]+3.2))*1.5)
            YR = np.cumsum(10**(events[MAGNITUDE_COLUMN]/1.5+9.1))/2e4
            YR_label = "Seismic Energy (J)"
            YR_name = "Seismic energy"


    timeline_fig = make_subplots(specs=[[{'secondary_y': True}]])

    timeline_fig.update_layout(
        yaxis=dict(title=YL_label),
        yaxis2=dict(title=YR_label))

    timeline_fig.add_trace(go.Scatter(x=XL,
                                      y=YL,
                                      mode='lines',
                                      name=YL_name,
                                      line_color='rgba(0, 128, 0, 0.3)',),)
    if YL2.size != 0:
        timeline_fig.add_trace(go.Scatter(x=XL,
                                          y=YL2,
                                          mode='lines',
                                          name=YL_name2,
                                          line_color='rgba(255, 165, 0, 0.5)', ), )

    timeline_fig.add_trace(go.Scatter(x=XR,
                                      y=YR,
                                      mode='lines',
                                      name=YR_name,
                                      line_shape = YR_line_shape,
                                      fill=YR_fill,
                                      line_color='#967387'), secondary_y=True)

    basic_style_2yaxes(timeline_fig)
    timeline_fig.update_layout(autosize=True,
                               height=800,
                               font=dict(family='Verdana',size=16,),
                               plot_bgcolor='white',
                               yaxis=dict(showgrid=False),
                               yaxis2=dict(showgrid=False),)


    return dcc.Graph(figure=timeline_fig)


def render():

    return html.Div([
        dmc.Title('Seismicity vs Hydraulics'),
        # html.H3('/!\ Disclaimer: Seiscomp magnitudes are \
        #     MLc, GES & Malmi magnitudes are Mw'),
        html.Br(),

        html.Table(style={'width': '99%', 'cellPadding': '1',
                          'cellSpacing': '1', 'border': '0'},
                   children=[
                       html.Tr(children=[
                           html.Td(children=[
                               dmc.Select(id='dataSelectSH',
                                          data=[
                                              {'label': 'Catalog: background catalog', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscomp-bg.csv')},
                                              {'label': 'Catalog: experimental catalog FEAR1', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscomp.csv')},
                                              {'label': 'Catalog: DD catalog FEAR1', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscomp-dd.csv')},
                                              {'label': 'Catalog: experimental catalog MZeroB', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscompM0B.csv')},
                                              {'label': 'Catalog: DD catalog MZeroB', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscomp-ddM0B.csv')},
                                              {'label': 'Catalog: experimental catalog MZeroA', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscompM0A.csv')},
                                              {'label': 'Catalog: DD catalog MZeroA', 'value': os.path.join(
                                                  assets_dir, 'catalogs', 'seiscomp-ddM0A.csv')}
                                          ],
                                          value=os.path.join(assets_dir, 'catalogs', 'seiscomp.csv'),
                                          clearable=False,
                                          style={'width': '100%', 'marginBottom': 10},
                                          ), ]
                           ), ]
                       ),
                       html.Tr(children=[
                           html.Td(children=[
                               dmc.Select(
                                   id='minPhasesSH',
                                   data=[
                                       {'label': f'Number of phases: Min {i} phases', 'value': i}
                                       for i in range(0, 17, 2)
                                   ],
                                   value=8,
                                   clearable=False,
                               ),
                           ]),
                       ]),
                   ]),
        html.Br(),
        dmc.Divider(variant="solid", size=5),
        html.Br(),
        html.Table(style={'width': '99%', 'cellPadding': '1',
                          'cellSpacing': '1', 'border': '0'},
                   children=[
                       html.Tr(children=[
                           html.Td(children=[
                               dmc.Text('Y axis left'),
                               dmc.Select(id='dataYL',
                                          data=[
                                              {'label': 'Flow rate', 'value': 'topflow'},
                                              {'label': 'Pressure', 'value': 'bottompressure'},
                                              {'label': 'Hydraulic Energy', 'value': 'energy'},

                                          ],
                                          value='topflow',
                                          clearable=False,
                                          ), ]
                           ),
                           html.Td(children=[
                               dmc.Text('Y axis right'),
                               dmc.Select(id='dataYR',
                                          data=[
                                              {'label': 'Seismicity rate (per minute)', 'value': 'rate'},
                                              {'label': 'Seismicity rate (per hour)', 'value': 'rate_hour'},
                                              {'label': 'Cum. number of events', 'value': 'cumulative'},
                                              {'label': 'Seismic energy', 'value': 'moment'},
                                          ],
                                          value='rate',
                                          clearable=False,
                                          ), ]
                           ),
                       ]),
                   ]),
        dmc.Container(
            dmc.Group(
                spacing=50,
                align='end',
                mt=20,
                children=[
                    dmc.DatePicker(
                        id='date-startSH',
                        label='Start Date',
                        value=(datetime.now(timezone.utc) - timedelta(hours=24))
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='Start Time',
                        id='time-startSH',
                        value=(datetime.now(timezone.utc) - timedelta(hours=24))
                        .replace(tzinfo=None, microsecond=0)),
                    dmc.DatePicker(
                        id='date-endSH',
                        label='End Date',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='End Time', id='time-endSH',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0)),
                    dmc.Button(
                        id='submit-button-stateSH',
                        n_clicks=0,
                        children='Submit'),
                ],
            )),
        dmc.RadioGroup(
            [dmc.Radio(l, value=k) for k, l in [["BFE05", 'BFE05'], ['BFE06', 'BFE06']]],
            id="radio-borehole-pressure3",
            value='BFE05',
            label="Select borehole pressure",
            size="sm",
            mt=20,
            style={
                'marginTop': '20px',
                'alignSelf': 'flex-start',
                'textAlign': 'left',
                'alignItems': 'flex-start'
            }
        ),
        dcc.Loading(
            id='loading-iconSH',
            children=[html.Div(id='plotlyscatterSH')],
            type='default',
            style={'marginTop': '50px', 'alignSelf': 'flex-start'})
    ])

layout = render