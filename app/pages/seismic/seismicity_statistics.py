import os
from datetime import datetime, timedelta, timezone
import numpy as np

import dash
import dash_mantine_components as dmc
import plotly.graph_objs as go
from plotly.subplots import make_subplots
from dash import Input, Output, callback, dcc, html, State, callback_context

from app.functions.fetch_catalogue import load_events
from app.functions.data_processing import (bvalue_since_start,
                                           calculate_FMD_data,
                                           transient_bvalue)
from app.functions.global_variables import MAGNITUDE_COLUMN, TIME_COLUMN, BINNING
from app.functions.plotting_functions import (add_catalogue_FMD_plot,add_trace_temporal_bvalue)


dash.register_page(__name__, name='Seismicity Statistics', title='Seismicity Statistics')

assets_dir = os.path.abspath('app/assets')

@callback(
    [Output('stat-date-start', 'value'),
    Output('stat-date-end', 'value')],
    Input('stat-dataSelect', 'value')
)
def update_start_date(catalog):
    options = {
        os.path.join(assets_dir, 'catalogs', 'seiscomp.csv'): datetime(2024, 11, 25),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-dd.csv'): datetime(2024, 11, 25),
        os.path.join(assets_dir, 'catalogs', 'seiscompM0B.csv'): datetime(2024, 8, 20),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0B.csv'): datetime(2024, 8, 20),
        os.path.join(assets_dir, 'catalogs', 'seiscompM0A.csv'): datetime(2024, 4, 15),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0A.csv'): datetime(2024, 4, 15),
    }

    options_end = {
        os.path.join(assets_dir, 'catalogs', 'seiscomp.csv'): datetime.now(),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-dd.csv'): datetime.now(),
        os.path.join(assets_dir, 'catalogs', 'seiscompM0B.csv'): datetime(2024, 9, 10),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0B.csv'): datetime(2024, 9, 10),
        os.path.join(assets_dir, 'catalogs', 'seiscompM0A.csv'): datetime(2024, 5, 10),
        os.path.join(assets_dir, 'catalogs', 'seiscomp-ddM0A.csv'): datetime(2024, 5, 10),
    }
    return [options.get(catalog, datetime(2024, 1, 1)).replace(tzinfo=None, microsecond=0).strftime('%Y-%m-%d'),
            options_end.get(catalog, datetime(2024, 1, 1)).replace(tzinfo=None, microsecond=0).strftime('%Y-%m-%d')]

@callback(
    [Output('plotly_fmd', 'children'),Output('b_in_time', 'children')],
    [Input('stat-dataSelect', 'value'),
    Input('minPhases', 'value'),
    Input('MC_correction', 'value'),
    Input('stat-submit-button-state', 'n_clicks'),
    State('stat-date-start', 'value'),
    State('stat-time-start', 'value'),
    State('stat-date-end', 'value'),
    State('stat-time-end', 'value')]
)
def update_data_and_plots(selected_catalog, min_phases, correction,n_clicks,
                 date_start,
                 time_start,
                 date_end,
                 time_end):

    binning = BINNING

    # Fetch the latest value of all inputs
    ctx = callback_context
    triggered_inputs = {trigger["prop_id"].split(".")[0]: trigger["value"] for trigger in ctx.triggered}

    # Ensure start date and time are up-to-date
    if 'stat-dataSelect' in triggered_inputs:
        # This ensures the start date and time are updated based on the catalog
        TS,TE = update_start_date(selected_catalog)
        date_start = datetime.strptime(TS,'%Y-%m-%d')
        time_start = datetime.strptime(TS,'%Y-%m-%d')
        date_end = datetime.strptime(TE,'%Y-%m-%d')
        time_end = datetime.strptime(TE,'%Y-%m-%d')
    else:
        time_start = datetime.strptime(time_start, '%Y-%m-%dT%H:%M:%S')
        date_start = datetime.strptime(date_start, '%Y-%m-%d')
        time_end = datetime.strptime(time_end, '%Y-%m-%dT%H:%M:%S')
        date_end = datetime.strptime(date_end, '%Y-%m-%d')

    start = datetime.combine(date_start.date(), time_start.time())
    end = datetime.combine(date_end.date(), time_end.time())

    if selected_catalog == os.path.join(
            assets_dir, 'catalogs', 'seiscomp.csv'):
        seiscomp_catalogue = load_events(selected_catalog, 0.01, min_phases)
        #events = load_events_fdsn(FDSNclient(FDSN_URL_EXP + '/fdsnws/event/1/query'), start, end, 0.01, min_phases,
        #                           rotation=True)
    else:
        #events = load_events_from_csv(selected_catalog)
        seiscomp_catalogue = load_events(selected_catalog, 0.01, min_phases)


    seiscomp_catalogue=seiscomp_catalogue[seiscomp_catalogue[TIME_COLUMN].dt.tz_localize(None)>=np.datetime64(start)]
    seiscomp_catalogue=seiscomp_catalogue[seiscomp_catalogue[TIME_COLUMN].dt.tz_localize(None)<=np.datetime64(end)]


    #seiscomp_catalogue = load_events(selected_catalog, binning, min_phases)

    # Filter out magnitudes below -4.9
    # /!\ this is a temporary fix

    #seiscomp_catalogue = seiscomp_catalogue[seiscomp_catalogue[MAGNITUDE_COLUMN] >= -4.9]

    # Sub-catalogue with latest 1000 events (Seiscomp)
    latest_1000_seiscomp_catalogue = seiscomp_catalogue.iloc[-1000:]
    # Sub-catalogue with latest 24h events (Seiscomp)
    #latest_24h_seiscomp_catalogue = seiscomp_catalogue[
    #    seiscomp_catalogue[TIME_COLUMN] >=
    #    seiscomp_catalogue[TIME_COLUMN].max()
    #    - np.timedelta64(1, 'D')]

    # Calculate FMD and statistical parameters for each catalogue
    fmd_dict_seiscomp = calculate_FMD_data(seiscomp_catalogue,correction=correction)
    fmd_dict_latest_1000_seiscomp = calculate_FMD_data(
        latest_1000_seiscomp_catalogue,correction=correction)
    #fmd_dict_latest_24h_seiscomp = calculate_FMD_data(
    #    latest_24h_seiscomp_catalogue)

    # FMD figure
    fig1 = go.Figure()
    # Seiscomp catalogue
    add_catalogue_FMD_plot(fig1, fmd_dict_seiscomp, 'green', 'Seiscomp')
    # Latest 1000 events Seiscomp catalogue
    add_catalogue_FMD_plot(fig1, fmd_dict_latest_1000_seiscomp,
                           'blue', 'Latest 1000 events')
    # Latest 24h events Seiscomp catalogue
    #add_catalogue_FMD_plot(fig1, fmd_dict_latest_24h_seiscomp,
    #                      'pink', 'Latest 24h Seiscomp')
    fig1.update_layout(xaxis_title='Magnitude',
                       yaxis_title='Number of events')
    # styling
    fig1.update_yaxes(type='log')

    Nevents = 100
    overlap_window = 0.1
    # Hardcoded time interval, could be fetched from forecast series if desired
    time_interval_in_minutes = 10

    # Transient b-value
    transient_b_seiscomp = transient_bvalue(
        seiscomp_catalogue,
        fmd_dict_seiscomp['catalogue_mc'],
        Nevents,
        int(Nevents*overlap_window),
        binning,
        TIME_COLUMN,
        MAGNITUDE_COLUMN)

    fig2 = make_subplots(specs=[[{'secondary_y': True}]])
    add_trace_temporal_bvalue(fig2, seiscomp_catalogue, transient_b_seiscomp,
                              'green', 'Seiscomp')

    fig2.update_layout(xaxis_title='Time',
                       yaxis_title='b-value',
                       yaxis2_title='Cumulative number of events',
                       title='Transient b-value')

    graph_style = {
        'height': '70vh',
        'minHeight': '400px',
        'display': 'inline-block',
        'width': '100%'
    }

    return [dcc.Graph(figure=fig1, style=graph_style),dcc.Graph(figure=fig2,style=graph_style)]

def render():

    return html.Div([
    dmc.Title('Catalogue statistics'),
    html.H3(
            f'Mc calculated with maximum curvature method \
          (with selectable correction factor); \
          b-value calculated with MLE with correction for binning;'
         # window size: {nevents} events, overlap: \
         # {overlap_window * 100} %'
    ),
    html.Br(),
    html.Table(style={'width': '99%', 'cellPadding': '1',
                      'cellSpacing': '1', 'border': '0'}, children=[
        html.Tr(children=[
            html.Td(children=[
                dmc.Select(
                    id='stat-dataSelect',
                    data=[
                        {'label': 'Catalog: experimental catalog FEAR1', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscomp.csv')},
                        {'label': 'Catalog: DD catalog FEAR1', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscomp-dd.csv')},
                        {'label': 'Catalog: experimental catalog MZeroB', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscompM0B.csv')},
                        {'label': 'Catalog: DD catalog MZeroB', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscomp-ddM0B.csv')},
                        {'label': 'Catalog: experimental catalog MZeroA', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscompM0A.csv')},
                        {'label': 'Catalog: DD catalog MZeroA', 'value': os.path.join(
                            assets_dir, 'catalogs', 'seiscomp-ddM0A.csv')}
                    ],
                    value=os.path.join(
                        assets_dir, 'catalogs', 'seiscomp.csv'),
                    clearable=False,
                ),
            ])
        ]),
        html.Tr(children=[
            html.Td(children=[
                dmc.Select(
                    id='minPhases',
                    data=[
                        {'label': f'Number of phases: Min {i} phases', 'value': i}
                        for i in range(0, 17, 2)
                    ],
                    value=8,
                    clearable=False,
                ),
            ]),
        ]),
        html.Tr(children=[
            html.Td(children=[
                html.Br(),
                dmc.Text('Mc MAXC correction:',size="sm"),
                dmc.NumberInput(
                    id='MC_correction',
                    value=0.2,
                    min = -10,
                    step = 0.1,
                    precision=2
                ),
            ],style={'display': 'inline-block', 'textAlign': 'left'})
        ],style={'textAlign': 'left'}),
        dmc.Container(
            dmc.Group(
                spacing=50,
                align='end',
                mt=20,
                children=[
                    dmc.DatePicker(
                        id='stat-date-start',
                        label='Start Date',
                        value=(datetime(2024, 11, 25))
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='Start Time',
                        id='stat-time-start',
                        value=(datetime(2024, 11, 25))
                        .replace(tzinfo=None, microsecond=0),
                    ),
                    dmc.DatePicker(
                        id='stat-date-end',
                        label='End Date',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0).date(),
                        style={'width': 200},
                    ),
                    dmc.TimeInput(
                        label='End Time', id='stat-time-end',
                        value=datetime.now(timezone.utc)
                        .replace(tzinfo=None, microsecond=0)
                    ),
                    dmc.Button(
                        id='stat-submit-button-state',
                        n_clicks=0,
                        children='Submit'),
                ],
            )),

        dcc.Loading(
            id='loading-iconSH',
            children=[html.Div(id='plotly_fmd')],
            type='default',
            style={'width': '100%'}),
        dcc.Loading(
            id='loading-iconSH',
            children=[html.Div(id='b_in_time')],
            type='default',
            style={'width': '100%'})
    ])
])

layout = render
