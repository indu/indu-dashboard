import numpy as np
import pandas as pd
import os
from seismostats.utils.binning import bin_to_precision

from app.functions.coordinates import CoordinateTransformer
from app.functions.global_variables import (MAGNITUDE_COLUMN,
                                            NUM_PHASES_COLUMN, TIME_COLUMN,
                                            MIN_LAT,MAX_LAT,MIN_LON,MAX_LON,
                                            PROJ_STRING,REF_E, REF_N, REF_A)

def load_events_fdsn(client, start_time, end_time, binning, number_phases=12,rotation = False):
    '''
    Load events from fdsn web service and bin magnitudes to precision,
    ensure time is given as a datetime object and sort events by time
    Args:
        client: fdsn client
        start_time: starting time
        end_time: starting time
        binning: magnitude binning of the catalogue
        number_phases: minimum number of phases for an event to be included
            (default=12)
    Returns:
        events: dataframe with columns TIME_COLUMN and MAGNITUDE_COLUMN
    '''

    events = client.get_events(start_time=start_time,
                               end_time=end_time,
                               min_latitude=MIN_LAT,
                               max_latitude=MAX_LAT,
                               min_longitude=MIN_LON,
                               max_longitude=MAX_LON,
                               include_quality=True)
    events.rename(columns={'magnitude':MAGNITUDE_COLUMN,
                           'time':TIME_COLUMN,
                           'associatedphasecount':NUM_PHASES_COLUMN},inplace=True)

    events[MAGNITUDE_COLUMN] = bin_to_precision(
        events[MAGNITUDE_COLUMN], binning)

    # make sure time is given as a datetime object
    if isinstance(events[TIME_COLUMN][0], str):
        events[TIME_COLUMN] = pd.to_datetime(events[TIME_COLUMN])
    # sort events by time
    events = events.sort_values(by=TIME_COLUMN)

    # filter out events with NaN magnitude
    events = events[~events[MAGNITUDE_COLUMN].isna()]

    # filter events above number of phases threshold
    if NUM_PHASES_COLUMN in events.columns:
        events = events[events[NUM_PHASES_COLUMN] >= number_phases]

    events = events[events['event_type'] != 'other event']

    if rotation == True:
        transformer = CoordinateTransformer(PROJ_STRING,REF_E,REF_N,0)
        swiss_coord = transformer.to_local_coords(
            events['longitude'],
            events['latitude'],
            events['depth'])
        events['x']=swiss_coord[0]
        events['y']= swiss_coord[1]
        events['z']= -swiss_coord[2]-REF_A

    return events

def load_events(event_url, binning, number_phases=12):
    '''
    Load events from a csv file and bin magnitudes to precision,
    ensure time is given as a datetime object and sort events by time
    Args:
        event_url: path to the csv file
        binning: magnitude binning of the catalogue
        number_phases: minimum number of phases for an event to be included
            (default=12)
    Returns:
        events: dataframe with columns TIME_COLUMN and MAGNITUDE_COLUMN
    '''
    events = pd.read_csv(event_url)
    # bin magnitudes to precision
    events[MAGNITUDE_COLUMN] = bin_to_precision(
        events[MAGNITUDE_COLUMN], binning)
    # make sure time is given as a datetime object
    if isinstance(events[TIME_COLUMN][0], str):
        events[TIME_COLUMN] = pd.to_datetime(events[TIME_COLUMN])
    # sort events by time
    events = events.sort_values(by=TIME_COLUMN)

    # filter out events with NaN magnitude
    events = events[~events[MAGNITUDE_COLUMN].isna()]

    # filter events above number of phases threshold
    if NUM_PHASES_COLUMN in events.columns:
        events = events[events[NUM_PHASES_COLUMN] >= number_phases]

    return events

def load_events_ges(event_url):
    events = pd.read_csv(event_url)
    events.rename(
        columns={' MomMag': MAGNITUDE_COLUMN, '      X     ': 'x', '      Y     ':
                 'y', '    Depth   ': 'z'}, inplace=True)
    events[MAGNITUDE_COLUMN] = np.round(pd.to_numeric(events[MAGNITUDE_COLUMN]), decimals=7)
    events = events[events[MAGNITUDE_COLUMN] > -20]
    events[TIME_COLUMN] = pd.to_datetime(
        events['Origin Date'] + events['   Origin Time  '] + '+0600',
        format='mixed', dayfirst=True, utc=True)

    events['x'] = events['x']*0.3048-906.26925833102
    events['y'] = events['y']*0.3048+335.452428309492
    events['z'] = -events['z']*0.3048+1650.0249

    if isinstance(events[TIME_COLUMN], str):
        events[TIME_COLUMN] = pd.to_datetime(events[TIME_COLUMN])
    # sort events by time
    events = events.sort_values(by=TIME_COLUMN)

    return events


def load_events_from_csv(event_url):
    events = pd.read_csv(event_url)
    # events['time'] = pd.to_datetime(events['isotime'])
    return events

def merge_ges_files(assets_dir):

    path = os.path.join(assets_dir, 'catalogs', 'GES2024')
    if not os.path.exists(path):
        return None
    files = os.listdir(path)

    dfs = []
    # Append all the files as dataframes to the first one
    for file in files:
        df_csv = pd.read_csv(os.path.join(path, file))
        dfs.append(df_csv)

    df = pd.concat(dfs, ignore_index=True)
    df.to_csv(os.path.join(assets_dir, 'catalogs', 'ges.csv'))