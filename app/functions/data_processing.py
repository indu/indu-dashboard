from datetime import timedelta

import numpy as np
import pandas as pd
from scipy.stats import poisson
from seismostats import estimate_b
from seismostats.analysis.bvalue import shi_bolt_confidence
from seismostats.analysis.estimate_mc import mc_max_curvature
from seismostats.utils.binning import bin_to_precision, get_cum_fmd, get_fmd
from sklearn.neighbors import NearestNeighbors

from app.functions.global_variables import (BINNING, MAG_ORANGE_TLS,
                                            MAG_RED_TLS, MAGNITUDE_COLUMN,
                                            MIN_NUM_PHASES,
                                            SEISMOSTATS_MAGNITUDE_COLUMN,
                                            SEISMOSTATS_PHASE_COLUMN,
                                            SEISMOSTATS_TIME_COLUMN,
                                            TIME_COLUMN)

# method to calculate the b-value based on the N nearest neighbours in space using sklearn NearestNeighbors function


def spatial_bvalue(catalogue_dataframe, N, cluster=None):
    '''
    method to calculate the b-value based on the N nearest neighbours in space using sklearn NearestNeighbors function
    returns: for each event: b, Mc, error on b
    '''
    MAGNITUDE = MAGNITUDE_COLUMN
    XCOORD = 'x'
    YCOORD = 'y'
    ZCOORD = 'z'
    catalogue_dataframe['ind'] = catalogue_dataframe.index

    MC_col = 'Mc'
    b_col = 'b-value'
    b_std_col = 'std b-value'
    spatial_results = pd.DataFrame(
        columns=['ind', XCOORD, YCOORD, ZCOORD, MAGNITUDE, MC_col, b_col, b_std_col])

    catalogue_dataframe = catalogue_dataframe.reset_index()

    if N > len(catalogue_dataframe):
        N = len(catalogue_dataframe)
        # print('Warning: reduced the number of neighbours to ' + str(N) + ' as the catalog is too small')

    nearest_neighbours = NearestNeighbors(n_neighbors=N, algorithm='kd_tree', metric='euclidean').fit(
        catalogue_dataframe[[XCOORD, YCOORD, ZCOORD]])
    distances, indices = nearest_neighbours.kneighbors(
        catalogue_dataframe[[XCOORD, YCOORD, ZCOORD]])

    for index, event in catalogue_dataframe.iterrows():

        new_line = dict()
        new_line[XCOORD] = event[XCOORD]
        new_line[YCOORD] = event[YCOORD]
        new_line[ZCOORD] = event[ZCOORD]
        new_line[MAGNITUDE] = event[MAGNITUDE]
        new_line['ind'] = event['ind']

        magnitudes_neighbours = catalogue_dataframe[MAGNITUDE].iloc[indices[index][:]]
        magnitudes_neighbours = bin_to_precision(
            magnitudes_neighbours, delta_x=BINNING)

        new_line[MC_col] = mc_max_curvature(sample=magnitudes_neighbours.to_numpy(), delta_m=BINNING,
                                            correction_factor=0.025)

        ind_discarded = []
        if len(magnitudes_neighbours[magnitudes_neighbours >= new_line[MC_col]]) >= 100:
            new_line[b_col] = estimate_b(
                magnitudes=magnitudes_neighbours[magnitudes_neighbours >= new_line[MC_col]].to_numpy(
                ),
                mc=new_line[MC_col], b_parameter="b_value", delta_m=BINNING)
            new_line[b_std_col] = shi_bolt_confidence(
                magnitudes=magnitudes_neighbours[magnitudes_neighbours >= new_line[MC_col]].to_numpy(
                ),
                b=new_line[b_col], b_parameter="b_value")
        else:
            ind_discarded.append(index)
            new_line[b_col] = np.nan
            new_line[b_std_col] = np.nan

        spatial_results.loc[len(spatial_results)] = new_line

    # print(str(round(len(ind_discarded) / len(catalogue_dataframe) * 100, 4)) + '% of events discarded')
    spatial_results = spatial_results.set_index('ind')
    return spatial_results


def calculate_FMD_data(catalogue_dataframe, binning=BINNING,
                       mag_column=MAGNITUDE_COLUMN, time_column=TIME_COLUMN,
                       correction=0.2):
    '''
    Calculate the Frequency-Magnitude Distribution (FMD) of the catalogue
    Args:
        catalogue_dataframe: dataframe with at minumum columns
            mag_column and time_column
        binning: magnitude binning of the catalogue, default is BINNING
        mag_column: name of the column with the magnitudes, default is
            MAGNITUDE_COLUMN
        time_column: name of the column with the times, default is TIME_COLUMN
    Returns:
        FMD_dict: dictionary with the FMD and statistical parameters of the
            catalogue
    '''
    # bin catalogue to precision
    catalogue_dataframe[mag_column] = bin_to_precision(
        catalogue_dataframe[mag_column], binning)
    catalogue_bins, catalogue_count, catalogue_mags = get_fmd(
        catalogue_dataframe[MAGNITUDE_COLUMN].values, binning,
        bin_position='center')
    catalogue_cum_bins, catalogue_cum_count, catalogue_mags = get_cum_fmd(
        catalogue_dataframe[MAGNITUDE_COLUMN].values, binning,
        bin_position='center')
    # statistical parameters
    catalogue_mc = mc_max_curvature(
        catalogue_dataframe[MAGNITUDE_COLUMN].values, delta_m=binning,correction_factor=correction)
    catalogue_bvalue = round(estimate_b(catalogue_dataframe[MAGNITUDE_COLUMN].values[
        catalogue_dataframe[MAGNITUDE_COLUMN].values >= catalogue_mc],
        catalogue_mc, delta_m=binning), 2)
    catalogue_std_bvalue = round(shi_bolt_confidence(
        catalogue_dataframe[MAGNITUDE_COLUMN].values[
            catalogue_dataframe[MAGNITUDE_COLUMN].values >= catalogue_mc],
        catalogue_bvalue, b_parameter='b_value'), 2)

    string_stats_catalogue = 'Mc = ' + str(round(catalogue_mc, 2)) + ', b = ' + \
        str(round(catalogue_bvalue, 2)) + ' \u00B1 ' + \
        str(round(catalogue_std_bvalue, 3))

    GR_fit_catalogue = gutenberg_richter(
        catalogue_cum_bins[catalogue_cum_bins >= catalogue_mc],
        catalogue_bvalue, min(
            catalogue_cum_bins[catalogue_cum_bins >= catalogue_mc]),
        len(catalogue_dataframe[MAGNITUDE_COLUMN].values[
            catalogue_dataframe[MAGNITUDE_COLUMN].values >= catalogue_mc]))

    FMD_dict = {'catalogue_bins': catalogue_bins,
                'catalogue_count': catalogue_count,
                'catalogue_cum_bins': catalogue_cum_bins,
                'catalogue_cum_count': catalogue_cum_count,
                'catalogue_mags': catalogue_mags,
                'catalogue_mc': catalogue_mc,
                'catalogue_bvalue': catalogue_bvalue,
                'catalogue_std_bvalue': catalogue_std_bvalue,
                'string_stats_catalogue': string_stats_catalogue,
                'GR_fit_catalogue': GR_fit_catalogue}

    return FMD_dict


def transient_bvalue(catalogue_dataframe, Mc, window, increment,
                     binning, time_column_name='time',
                     magnitude_column_name='magnitude'):
    '''
    calculates the b-value for each window (fixed number of events) and
    the time of the last event of the window
    Args:
        catalogue_dataframe: dataframe with at minumum columns
            time_column_name and magnitude_column_name
        Mc: completeness magnitude
        window: size of the window in number of events
        increment: number of events to move the window (in percentage of
            overlap)
        binning: magnitude binning of the catalogue

    Returns:
        transient_results: dataframe with columns 'b-value', 'time'
            and 'std b-value'
    '''
    condition_completeness = catalogue_dataframe[magnitude_column_name] >= Mc
    complete_catalogue = catalogue_dataframe[condition_completeness]
    transient_results = pd.DataFrame(
        columns=['b-value', 'time', 'std b-value'])
    for index in range(window, len(complete_catalogue), increment):
        new_line = dict()
        magnitudes_events_in_window = complete_catalogue[
            magnitude_column_name][index - window:index]
        times_events_in_window = complete_catalogue[
            time_column_name][index - window:index]
        new_line['time'] = times_events_in_window.max()
        new_line['b-value'] = round(estimate_b(magnitudes_events_in_window[
            magnitudes_events_in_window >=
            Mc].to_numpy(), Mc, delta_m=binning), 2)
        new_line['std b-value'] = round(shi_bolt_confidence(
            magnitudes=magnitudes_events_in_window[
                magnitudes_events_in_window >= Mc].to_numpy(),
            b=new_line['b-value'], b_parameter="b_value"), 2)
        transient_results.loc[len(transient_results)] = new_line
    return transient_results


def bvalue_since_start(catalogue_dataframe, Mc, binning, time_interval,
                       minimum_number_of_events=100, time_column_name='time',
                       magnitude_column_name='magnitude'):
    '''
    calculates the evolution of the b-value since the start with updates
    every time_interval minutes
    Args:
        catalogue_dataframe: dataframe with at minumum columns
            time_column_name and magnitude_column_name
        Mc: completeness magnitude
        binning: magnitude binning of the catalogue
        minimum_number_of_events: minimum number of events to
            calculate the b-value
        time_interval: time interval in minutes

    Returns:
        transient_results: dataframe with columns 'b-value', 'time'
            and 'std b-value'
    '''
    condition_completeness = catalogue_dataframe[magnitude_column_name] >= Mc
    complete_catalogue = catalogue_dataframe[condition_completeness]
    transient_results = pd.DataFrame(
        columns=['b-value', 'time', 'std b-value'])
    starttime = complete_catalogue[time_column_name].min()
    endtime = complete_catalogue[time_column_name].max()
    # create a time range with the time_interval
    time_range = pd.date_range(
        start=starttime, end=endtime, freq=f'{time_interval}min')
    for index in range(len(time_range)):
        new_line = dict()
        magnitudes_events_in_window = complete_catalogue[
            complete_catalogue[time_column_name]
            < time_range[index]][magnitude_column_name]
        if len(magnitudes_events_in_window) > minimum_number_of_events:
            new_line['time'] = time_range[index]
            new_line['b-value'] = estimate_b(
                magnitudes_events_in_window[magnitudes_events_in_window >=
                                            Mc].to_numpy(), Mc,
                delta_m=binning)
            new_line['std b-value'] = shi_bolt_confidence(
                magnitudes=magnitudes_events_in_window[
                    magnitudes_events_in_window >= Mc].to_numpy(
                ),
                b=new_line['b-value'], b_parameter="b_value")
            transient_results.loc[len(transient_results)] = new_line
    return transient_results


def gutenberg_richter(magnitudes: np.ndarray, b_value: float,
                      mc: float, n_mc: int) -> np.ndarray:
    """ Estimates the cumulative Gutenberg richter law (proportional to the
    complementary cumulative FMD) for a given magnitude vector.

    Args:
        magnitudes: vector of magnitudes
        b_value: theoretical b_value
        mc: completeness magnitude
        n_mc: cumulative number of all events larger than the completeness
            magnitude (n_mc = 10 ** a)
    """
    return n_mc * 10 ** (-b_value * (magnitudes - mc))


def gutenberg_richter_interp_forecast(model_forecast, model_fit, seismicity):
    '''
    Calculate the Gutenberg-Richter law for the forecast
    Args:
        model_forecast: dataframe with the forecasted rates
        model_fit: dataframe with the fit of the model
    Returns:
        GR_y: dataframe with the fit of the Gutenberg-Richter
            law indexed by the magnitude bins
    '''

    # Filter seismicity by removing NaN values, and events with less than
    # MIN_NUM_PHASES
    seismicity = seismicity[seismicity[SEISMOSTATS_MAGNITUDE_COLUMN].notna()]
    if SEISMOSTATS_PHASE_COLUMN in seismicity.columns:
        seismicity = seismicity[seismicity[SEISMOSTATS_PHASE_COLUMN]
                                >= MIN_NUM_PHASES]
    seismicity_cum_bins, seismicity_cum_count, seismicity_mags = get_cum_fmd(
        seismicity[SEISMOSTATS_MAGNITUDE_COLUMN].values, BINNING,
        bin_position='center')
    model_N = model_fit['number_events'].sum() \
        + model_forecast['number_events'].sum()
    model_b = model_forecast['b'].median()
    model_b_std = model_forecast['b_std'].median()
    model_mc = model_forecast['mc'].median()

    model_N_minus = (model_fit['number_events'].sum() +
                     np.sum(model_forecast['number_events'] - 2*model_forecast['number_events_std']))
    model_N_plus = (model_fit['number_events'].sum() +
                    np.sum(model_forecast['number_events'] + 2 * model_forecast['number_events_std']))

    a_value = np.log10(model_fit['number_events'].sum()) + \
        model_forecast['b']*model_forecast['mc']
    M_min = np.min(seismicity[SEISMOSTATS_MAGNITUDE_COLUMN])
    M_max = max(np.max(seismicity[SEISMOSTATS_MAGNITUDE_COLUMN]), MAG_RED_TLS)
    GR_m = np.linspace(M_min - BINNING, M_max + BINNING,
                       round(abs(M_max - M_min) / BINNING))

    model_y = np.power(10., np.median(a_value) - GR_m * model_b)
    error25a = np.percentile(a_value, axis=0, q=25)
    error75a = np.percentile(a_value, axis=0, q=75)
    error25b = np.percentile(model_forecast['b'], axis=0, q=25)
    error75b = np.percentile(model_forecast['b'], axis=0, q=75)
    error25 = np.power(10., error25a - GR_m * error25b)
    error75 = np.power(10., error75a - GR_m * error75b)

    GR_y = gutenberg_richter(GR_m, model_b, model_mc, model_N)
    GR_y_minus = gutenberg_richter(GR_m, model_b, model_mc, model_N_minus)
    GR_y_plus = gutenberg_richter(GR_m, model_b, model_mc, model_N_plus)
    # GR_y = pd.DataFrame({'GR_y': GR_y}, index=seismicity_cum_bins[
    #    seismicity_cum_bins >= model_mc])
    # *np.ones((len(GR_m), 1))
    GR_y = pd.DataFrame({'GR_y': GR_y,
                         'GR_y_minus': GR_y_minus,
                         'GR_y_plus': GR_y_plus,
                         'GR_y_fit': model_y,
                         'errfit25': error25,
                         'errfit75': error75,
                         'b': model_b,
                         'b_std': model_b_std,
                         'mc': model_mc
                         }, index=GR_m)

    return GR_y


def Pocc(a, b, M):
    tr = 1/(10**(a-b*M)+1e-10)
    return 1-np.exp(-1/tr)


def calculate_Pocc_forecast(forecast, fit, cum='rates'):
    # timestep forecast
    if len(forecast) == 1:
        timestep_forecast = timedelta(seconds=1)
    else:
        timestep_forecast = forecast.index[1]-forecast.index[0]
    # timestep_forecast in hours
    number_of_seconds_in_hour = timestep_forecast.total_seconds()
    timestep_forecast = timedelta.total_seconds(
        timestep_forecast)/number_of_seconds_in_hour
    times = forecast.index

    model_b = forecast['b'].mean()
    model_mc = forecast['mc'].mean()

    model_N = forecast['number_events'].values / timestep_forecast
    model_N[model_N <= 0] = 1e-10
    model_N_minus = (forecast['number_events'].values - 2 * forecast[
        'number_events_std'].values) / timestep_forecast
    model_N_minus[model_N_minus <= 0] = 1e-10
    model_N_plus = (forecast['number_events'].values + 2 * forecast[
        'number_events_std'].values) / timestep_forecast
    model_N_plus[model_N_plus <= 0] = 1e-10

    # hourly a-value for the forecast (and 2 std for 95% confidence interval)
    if cum == 'past':
        a_hourly = np.log10(np.cumsum(model_N) +
                            fit['number_events'].sum()) + model_b*model_mc
        a_hourly_minus = np.log10(np.cumsum(model_N_minus)+fit['number_events'].sum()) + \
            model_b*model_mc
        a_hourly_plus = np.log10(np.cumsum(model_N_plus)+fit['number_events'].sum()) + \
            model_b*model_mc
    elif cum == 'cum':
        a_hourly = np.log10(np.cumsum(model_N)) + model_b*model_mc
        a_hourly_minus = np.log10(np.cumsum(model_N_minus)) + \
            model_b*model_mc
        a_hourly_plus = np.log10(np.cumsum(model_N_plus)) + \
            model_b*model_mc
    else:
        a_hourly = np.log10(model_N) + model_b*model_mc
        a_hourly_minus = np.log10(model_N_minus) + \
            model_b*model_mc
        a_hourly_plus = np.log10(model_N_plus) + \
            model_b*model_mc

    # probability of occurence of an orange event
    Pr_orange = Pocc(a_hourly, model_b, MAG_ORANGE_TLS)
    Pr_orange_minus = Pocc(a_hourly_minus, model_b, MAG_ORANGE_TLS)
    Pr_orange_plus = Pocc(a_hourly_plus, model_b, MAG_ORANGE_TLS)

    # probability of occurence of a red event
    Pr_red = Pocc(a_hourly, model_b, MAG_RED_TLS)
    Pr_red_minus = Pocc(a_hourly_minus, model_b, MAG_RED_TLS)
    Pr_red_plus = Pocc(a_hourly_plus, model_b, MAG_RED_TLS)

    df_Pocc = pd.DataFrame({'Pr_orange': Pr_orange,
                            'Pr_orange_minus': Pr_orange_minus,
                           'Pr_orange_plus': Pr_orange_plus,
                            'Pr_red': Pr_red, 'Pr_red_minus': Pr_red_minus,
                            'Pr_red_plus': Pr_red_plus}, index=times)

    return df_Pocc


def calculate_IG(LLe_model_1, LLe_model_2):
    '''
    Calculate the information gain between two models using model 1 as
    reference
    Args:
        LLe_model_1: dataframe with columns 'log_likelihood',
            'cumulative_log_likelihood' indexed by timesteps of the fit for
            model 1
        LLe_model_2: dataframe with columns 'log_likelihood',
            'cumulative_log_likelihood' indexed by timesteps of the fit for
            model 2
    Returns:
        information_gain: dataframe with columns 'information_gain' indexed
            by timesteps of the fit
    '''
    IG = LLe_model_2['cumulative_log_likelihood'] - \
        LLe_model_1['cumulative_log_likelihood']
    IG = pd.DataFrame({'information_gain': IG}, index=LLe_model_1.index)
    return IG


def seismicity_rate_model_fit(model_fit, model_seismicity, cum=False):
    '''
    Calculate the seismicity rate in the model fit
    Args:
        model_fit: dataframe with the model fit
        model_seismicity: dataframe with the seismicity of the model
    Returns:
        rate_above_Mc: dataframe with the number of events in each timebin
            indexed by the centers of the timebins
    '''
    # time bins in the model_fit is the center of the buckets
    # create timebins for the seismicity

    detla_time_fit = model_fit.index[1] - model_fit.index[0]
    # create timebins for the seismicity in increments of detla_time_fit
    start_timebins = model_fit.index[0] - detla_time_fit/2
    timebins = [start_timebins + i *
                detla_time_fit for i in range(len(model_fit)+1)]

    # filter seismicity to remove NaN values
    model_seismicity = model_seismicity[model_seismicity[
        SEISMOSTATS_MAGNITUDE_COLUMN].notna()]
    # filter seismicity to remove events with less than MIN_NUM_PHASES
    if SEISMOSTATS_PHASE_COLUMN in model_seismicity.columns:
        model_seismicity = model_seismicity[model_seismicity[
            SEISMOSTATS_PHASE_COLUMN] >= MIN_NUM_PHASES]
    # get seismicity rate on the same timesteps as the fit
    # Mc = mc_max_curvature(model_seismicity[SEISMOSTATS_MAGNITUDE_COLUMN],
    #                      delta_m=BINNING)
    Mc = model_fit['mc'].median()
    # count number of events in seismicity with M \geq Mc in each timebin
    rate_above_Mc = np.array([np.sum((model_seismicity[SEISMOSTATS_TIME_COLUMN]
                                      >= timebins[i]) & (
        model_seismicity[SEISMOSTATS_TIME_COLUMN] < timebins[i+1]) &
        (model_seismicity[SEISMOSTATS_MAGNITUDE_COLUMN] >= Mc))
        for i in range(len(timebins)-1)])

    # dataframe with the number of events in each timebin indexed by
    # timebin center (matches the model_fit indexes)
    if cum:
        rate_above_Mc = pd.DataFrame({'number_events': np.cumsum(rate_above_Mc)},
                                     index=model_fit.index)
    else:
        rate_above_Mc = pd.DataFrame({'number_events': rate_above_Mc},
                                     index=model_fit.index)

    return rate_above_Mc


def calculate_LLe(model_fit, model_seismicity,
                  upper_bound_num_events=500):
    '''
    Calculate the log likelihood of the model on the fit part of the data
    Args:
        model_fit: dataframe with the model fit
        model_seismicity: dataframe with the seismicity of the model
        upper_bound_num_events: upper bound of the number of events per bin
    Returns:
        LLe_df: dataframe with columns 'log_likelihood',
            'cumulative_log_likelihood' indexed by timesteps of the fit
    '''
    n_events_mc = seismicity_rate_model_fit(model_fit, model_seismicity)

    # assuming a possonian distribution, create a np.array containing the
    # probability of a certain number of events for each timestep
    Nevent_range = np.arange(0, upper_bound_num_events)
    proba_model = np.empty(
        (len(model_fit), upper_bound_num_events))
    for i in range(len(model_fit)):
        proba_model[i] = poisson.pmf(
            Nevent_range, model_fit['number_events'].values[i])
    # avoid singularity for 0 proba by setting it to a small value
    proba_model[proba_model == 0] = 1e-50

    # calculate log likelihood for each timestep and model
    log_likelihood_model = np.empty(len(model_fit))
    for i in range(len(model_fit)):
        log_likelihood_model[i] = np.sum(
            np.log(proba_model[i, n_events_mc['number_events'].values[i]]))
    # cumulative log likelihood
    cumulative_log_likelihood = np.cumsum(log_likelihood_model)

    LLe_df = pd.DataFrame({'log_likelihood': log_likelihood_model,
                           'cumulative_log_likelihood':
                          cumulative_log_likelihood},
                          index=n_events_mc.index)
    return LLe_df


def calculate_AIC(LLe_model, number_params_model):
    '''
    Calculate the Akaike Information Criterion (AIC) for a model
    Args:
        number_params_model: number of parameters of the model
        LLe_model: dataframe with columns 'log_likelihood',
            'cumulative_log_likelihood' indexed by timesteps of the fit
    Returns:
        AIC: Akaike Information Criterion
    '''
    max_c_LLe = LLe_model['cumulative_log_likelihood'].max()
    AIC = 2*number_params_model - 2 * max_c_LLe
    return AIC


def calculate_BIC(LLe_model, number_params_model, fit_model):
    '''
    Calculate the Bayesian Information Criterion (BIC) for a model
    Args:
        number_params_model: number of parameters of the model
        LLe_model: dataframe with columns 'log_likelihood',
            'cumulative_log_likelihood' indexed by timesteps of the fit
        fit_model: dataframe with the fit of the model
    Returns:
        BIC: Bayesian Information Criterion
    '''
    max_c_LLe = LLe_model['cumulative_log_likelihood'].max()
    BIC = number_params_model * np.log(len(fit_model)) - 2*max_c_LLe
    return BIC
