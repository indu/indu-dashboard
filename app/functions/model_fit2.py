from collections import OrderedDict

import dash
import dash_mantine_components as dmc
import pandas as pd
from dash import Input, Output, State, callback, dash_table, dcc, html
from hermes_client.clients import ForecastSeriesClient

from app.functions.components import ForecastSelector
from app.functions.data_access import fetch_forecast_modelruns, models_tbl
from app.functions.data_processing import (calculate_AIC, calculate_BIC,
                                           calculate_IG, calculate_LLe)
from app.functions.figure_styling import TLS_COLORS
from app.functions.global_variables import (MODEL_1_NAME, MODEL_2_NAME,
                                            NUMBER_PARAM, NUMBER_PARAM_MODEL_1,
                                            NUMBER_PARAM_MODEL_2, RAMSIS_URL)
from app.functions.plotting_functions import plot_cLLe, plot_IG, plot_LLe

# dash.register_page(__name__)

style = {
    'width': '100%',
    'display': 'inline-block',
    'height': '80vh',
    'minHeight': '500px'
}


@callback(Output("graph-container-fit", "children"),
          State({'type': 'select-fs', 'index': 'fs-fit'}, "value"),
          Input("slider-fc", "value"),
          prevent_initial_call=True)
def render(forecastseries_id, forecast_id):

    if not forecast_id or not forecastseries_id:
        return None

    fc_client = ForecastSeriesClient(url=RAMSIS_URL,
                                     forecastseries_id=forecastseries_id)

    forecast_id = int(forecast_id)

    modelruns = fetch_forecast_modelruns(fc_client, forecast_id)
    model_configs = models_tbl(fc_client)
    seismicity = fc_client.get_forecast_seismicity(forecast_id)

    models_name = list(model_configs.keys())
    models_fit = [next(fit for fit in modelruns if
                       fit['modelconfig_name'] == name)['rates_fit'] for name in models_name]

    # Calculate the log likelihood for each model
    LLe_models = [calculate_LLe(fit, seismicity) for fit in models_fit]

    # Calculate the information gain using model_1 as reference
    InfoGain = [calculate_IG(LLe_models[0], LLe) for LLe in LLe_models[1:]]

    # Calculate AIC/BIC for each model
    # TODO: check with Ryan if this is correct
    models_AIC = [calculate_AIC(LLe, NUMBER_PARAM[name])
                  for LLe, name in zip(LLe_models, models_name)]
    models_BIC = [calculate_BIC(LLe, NUMBER_PARAM[name], fit)
                  for LLe, name, fit in zip(LLe_models, models_name, models_fit)]

    # Create table with the model scores
    data_model_scores = OrderedDict(
        [
            ("Model", [name_model_1, name_model_2]),
            ("AIC", [round(model_1_AIC, 2), round(
                model_2_AIC, 2)]),
            ("BIC", [round(model_1_BIC, 2), round(
                model_2_BIC, 2)]),
        ]
    )
    df_model_scores = pd.DataFrame(data_model_scores)

    fig_LLe = plot_LLe(LLe_model_1, LLe_model_2, MODEL_1_NAME, MODEL_2_NAME)

    fig_cLLe = plot_cLLe(LLe_model_1, LLe_model_2, MODEL_1_NAME, MODEL_2_NAME)

    fig_IG = plot_IG(InfoGain)

    return dmc.Container([
        html.H1("Models fit"),
        html.H3("Log-likelihood and information gain for the fit period, \
            assuming poissonian distribution as output of the models"),

        dcc.Graph(figure=fig_LLe, style=style),
        dcc.Graph(figure=fig_cLLe, style=style),
        dcc.Graph(figure=fig_IG, style=style),
        html.H3(
            "positive IG: model 2 performs better | \
                negative IG: model 1 (reference) performs better"),
        dash_table.DataTable(data=df_model_scores.to_dict('records'),
                             columns=[
            {'id': c, 'name': c} for c in
            df_model_scores.columns],
            style_data_conditional=[
            {
                'if': {
                    'filter_query': '{{AIC}} = {}'.format(
                        df_model_scores['AIC'].min()),
                    'column_id': 'AIC'
                },
                'backgroundColor': TLS_COLORS[0],
                'color': 'white'
            },
            {
                'if': {
                    'filter_query': '{{BIC}} = {}'.format(
                        df_model_scores['BIC'].min()),
                    'column_id': 'BIC'
                },
                'backgroundColor': TLS_COLORS[0],
                'color': 'white'
            },
        ])
    ])


# Layout of the page
layout = dmc.Container([
    dmc.Title('Model fit'),
    ForecastSelector('fit'),
    dcc.Loading(
        id='loading-graph',
        children=[html.Div(id="graph-container-fit")],
        type="default",
        style={'marginTop': '50px', 'alignSelf': 'flex-start'})
])
