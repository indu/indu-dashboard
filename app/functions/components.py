from datetime import datetime

import dash_mantine_components as dmc
from dash import MATCH, Input, Output, callback, dcc, html
from hermes_client.clients import ForecastSeriesClient, HermesClient

from app.functions.global_variables import PROJECT_ID, RAMSIS_URL


class ForecastSelector(html.Div):

    def __init__(self, unique_id):
        self.unique_id = unique_id
        super().__init__([
            dcc.Loading(
                id=f'loading-select-{self.unique_id}',
                type='default',
                children=[
                    html.Div([
                        dmc.Select(
                            label='Select Forecastseries',
                            value='ng',
                            placeholder='Select Forecastseries',
                            id={'type': 'select-fs',
                                'index': f'fs-{self.unique_id}'},
                            data=[],
                            style={'width': 200, 'marginBottom': 10},
                        ),
                        dmc.Text(id={'type': 'select-fc',
                                     'id': f'fs-{self.unique_id}'}),
                    ])
                ]),
            dcc.Loading(
                id=f'loading-slider-{self.unique_id}',
                type='default',
                children=[
                    html.Div(
                        id={'type': 'select-fc',
                            'index': f'fs-{self.unique_id}'}
                    )
                ])
        ])

    @callback(Output({'type': 'select-fc', 'index': MATCH}, 'children'),
              Input({'type': 'select-fs', 'index': MATCH}, 'value'),
              prevent_initial_call=True)
    def render_slider(forecastseries_id):
        fc_client = ForecastSeriesClient(url=RAMSIS_URL,
                                         forecastseries_id=forecastseries_id)
        forecasts = fc_client.list_forecasts_info()

        forecasts = sorted(forecasts, key=lambda x: x['id'])

        marks = {}
        for fc in forecasts:
            if fc['status'] != 'COMPLETED':
                continue
            text = datetime.strftime(datetime.strptime(fc['starttime'],
                                                       '%Y-%m-%dT%H:%M:%S'),
                                     '%d/%m - %H:%M')

            style = {'transform': 'rotate(45deg) translate(5px, 15px)'}

            marks[fc['id']] = {'label': text, 'style': style}

        values = list(marks.keys())

        start = min(values)-0.1 if values else -1
        end = max(values)+len(forecasts)*0.1 if values else -1

        return [html.Div([
            html.Div([
                'Select a Forecastseries from '
                f"{forecasts[0]['starttime']} to "
                f"{forecasts[-1]['endtime']}"
            ], style={'margin': '10px auto'}),
            html.Div([
                dcc.Slider(
                    start, end,
                    step=None,
                    marks=marks,
                    value=None,
                    id='slider-fc',
                )
            ],
                style={'width': '90%', 'marginBottom': '30px'})
        ])]

    @callback(Output({'type': 'select-fs', 'index': MATCH}, 'data'),
              Input({'type': 'select-fs', 'index': MATCH}, 'value'))
    def get_forecastseries(input):
        client = HermesClient(url=RAMSIS_URL)

        forecastseries = client.list_forecastseries(project_id=PROJECT_ID)
        forecastseries = sorted(
            forecastseries, key=lambda x: x['id'], reverse=True)
        data_slider = [{'value': fc['id'], 'label': fc['name']}
                       for fc in forecastseries]
        return data_slider


class ForecastPlanSelector(html.Div):

    def __init__(self, unique_id):
        self.unique_id = unique_id

        super().__init__([
            dcc.Store(id={'type': 'uuid-map-fs-p',
                      'index': f'fs-{self.unique_id}'}),
            dcc.Loading(
                id=f'loading-select-{self.unique_id}',
                type='default',
                children=[
                    html.Div([
                        dmc.Select(
                            label='Select Forecastseries',
                            value='ng',
                            placeholder='Select Forecastseries',
                            id={'type': 'select-fs-p',
                                'index': f'fs-{self.unique_id}'},
                            data=[],
                            style={'width': 200, 'marginBottom': 10},
                        ),
                        dmc.Text(id={'type': 'select-fc-p',
                                 'id': f'fs-{self.unique_id}'}),
                        dmc.Text(id={'type': 'select-in-p',
                                 'id': f'fs-{self.unique_id}'}),
                    ])
                ]),
            dcc.Loading(
                id=f'loading-planmenu-{self.unique_id}',
                type='default',
                children=[html.Div([
                    html.Div(
                        id={'type': 'select-in-p',
                            'index': f'fs-{self.unique_id}'},
                        style={'display': 'inline-block',
                               'marginRight': '10px'}
                    )],
                    style={'textAlign': 'left'})
                ],
                style={'textAlign': 'left'}),
            dcc.Loading(
                id=f'loading-slider-{self.unique_id}',
                type='default',
                children=[
                    html.Div(
                        id={'type': 'select-fc-p',
                            'index': f'fs-{self.unique_id}'}
                    )
                ]),
        ])

    @callback([Output({'type': 'select-fc-p', 'index': MATCH}, 'children'),
               Output({'type': 'select-in-p', 'index': MATCH}, 'children')],
              Output({'type': 'uuid-map-fs-p', 'index': MATCH}, 'data'),
              Input({'type': 'select-fs-p', 'index': MATCH}, 'value'),
              prevent_initial_call=True)
    def render_slider(forecastseries_id):
        fc_client = ForecastSeriesClient(url=RAMSIS_URL,
                                         forecastseries_id=forecastseries_id)
        forecasts = fc_client.list_forecasts_info()

        forecasts = sorted(forecasts, key=lambda x: x['id'])

        marks = {}
        uuid_map = {}  # Mapping of indices to forecast UUIDs
        for i, fc in enumerate(forecasts):
            if fc['status'] != 'COMPLETED':
                continue
            text = datetime.strftime(datetime.strptime(fc['starttime'],
                                                       '%Y-%m-%dT%H:%M:%S'),
                                     '%d/%m - %H:%M')

            style = {'transform': 'rotate(45deg) translate(5px, 15px)'}

            marks[i] = {'label': text, 'style': style}
            uuid_map[i] = fc['id']  # Store UUID for this index

        values = list(marks.keys())

        start = min(values)-0.1 if values else -1
        end = max(values)+len(forecasts)*0.1 if values else -1

        injection_plans = fc_client.list_injectionplans()

        data_select = [{"label": ip['name'], "value": ip['id']}
                       for ip in injection_plans]

        initial = next(
            (ds for ds in data_select if 'planned' in ds['label']),
            data_select[0])

        # data_radio = [{"label": "Rates", "value": False},
        #               {"label": "Cumulative", "value": True}]

        return [html.Div([
            html.Div([
                'Select a Forecastseries from '
                f"{forecasts[0]['starttime']} to "
                f"{forecasts[-1]['endtime']}"
            ], style={'margin': '10px auto'}),
            html.Div([
                dcc.Slider(
                    start, end,
                    step=None,
                    marks=marks,
                    value=None,
                    id='slider-fc-p',
                )
            ],
                style={'width': '90%', 'marginBottom': '30px'})
        ])], [html.Div([
            html.Div([
                dmc.Select(
                    label='Select Injection Plan',
                    value=initial["value"],
                    placeholder=initial["label"],
                    id='select-in-p',
                    data=data_select,
                    style={'width': 200, 'marginBottom': 10},
                ),
            ],
                style={'width': '45%', 'marginBottom': '10px'})
        ])], uuid_map

    @callback(Output({'type': 'select-fs-p', 'index': MATCH}, 'data'),
              Input({'type': 'select-fs-p', 'index': MATCH}, 'value'))
    def get_forecastseries(input):
        client = HermesClient(url=RAMSIS_URL)

        forecastseries = client.list_forecastseries(project_id=PROJECT_ID)
        forecastseries = sorted(
            forecastseries, key=lambda x: x['id'], reverse=True)
        data_slider = [{'value': fc['id'], 'label': fc['name']}
                       for fc in forecastseries]
        return data_slider


class ForecastPlanStyleSelector(html.Div):

    def __init__(self, unique_id):
        self.unique_id = unique_id

        super().__init__([
            dcc.Store(id={'type': 'uuid-map-fs-ps',
                      'index': f'fs-{self.unique_id}'}),
            dcc.Loading(
                id=f'loading-select-{self.unique_id}',
                type='default',
                children=[
                    html.Div([
                        dmc.Select(
                            label='Select Forecastseries',
                            value='ng',
                            placeholder='Select Forecastseries',
                            id={'type': 'select-fs-ps',
                                'index': f'fs-{self.unique_id}'},
                            data=[],
                            style={'width': 200, 'marginBottom': 10},
                        ),
                        dmc.Text(id={'type': 'select-fc-ps',
                                     'id': f'fs-{self.unique_id}'}),
                        dmc.Text(id={'type': 'select-in-ps',
                                     'id': f'fs-{self.unique_id}'}),
                        dmc.Text(id={'type': 'cum-rates',
                                     'id': f'fs-{self.unique_id}'}),
                    ])
                ]),
            dcc.Loading(
                id=f'loading-planmenu-{self.unique_id}',
                type='default',
                children=[html.Div([
                    html.Div(
                        id={'type': 'select-in-ps',
                            'index': f'fs-{self.unique_id}'},
                        style={'display': 'inline-block',
                               'marginRight': '10px'}
                    ),
                    html.Div(
                        id={'type': 'cum-rates',
                            'index': f'fs-{self.unique_id}'},
                        style={'display': 'inline-block'}
                    )],
                    style={'textAlign': 'left'})
                ],
                style={'textAlign': 'left'}),
            dcc.Loading(
                id=f'loading-slider-{self.unique_id}',
                type='default',
                children=[
                    html.Div(
                        id={'type': 'select-fc-ps',
                            'index': f'fs-{self.unique_id}'}
                    )
                ]),
        ])

    @callback([Output({'type': 'select-fc-ps', 'index': MATCH}, 'children'),
               Output({'type': 'select-in-ps', 'index': MATCH}, 'children'),
               Output({'type': 'cum-rates', 'index': MATCH}, 'children')],
              Output({'type': 'uuid-map-fs-ps', 'index': MATCH}, 'data'),
              Input({'type': 'select-fs-ps', 'index': MATCH}, 'value'),
              prevent_initial_call=True)
    def render_slider(forecastseries_id):
        fc_client = ForecastSeriesClient(url=RAMSIS_URL,
                                         forecastseries_id=forecastseries_id)
        forecasts = fc_client.list_forecasts_info()

        forecasts = sorted(forecasts, key=lambda x: x['starttime'])

        marks = {}
        uuid_map = {}  # Mapping of indices to forecast UUIDs
        for i, fc in enumerate(forecasts):
            if fc['status'] != 'COMPLETED':
                continue
            text = datetime.strftime(datetime.strptime(fc['starttime'],
                                                       '%Y-%m-%dT%H:%M:%S'),
                                     '%d/%m - %H:%M')

            style = {'transform': 'rotate(45deg) translate(5px, 15px)'}

            marks[i] = {'label': text, 'style': style}
            uuid_map[i] = fc['id']  # Store UUID for this index

        values = list(marks.keys())

        start = min(values)-0.1 if values else -1
        end = max(values)+len(forecasts)*0.1 if values else -1

        injection_plans = fc_client.list_injectionplans()

        data_select = [{"label": ip['name'], "value": ip['id']}
                       for ip in injection_plans]

        initial = next(
            (ds for ds in data_select if 'planned' in ds['label']),
            data_select[0])

        data_radio = [{"label": "Rates", "value": "rates"},
                      {"label": "Cumulative", "value": "cum"},
                      {"label": "Cumulative past", "value": "past"}]

        return [html.Div([
            html.Div([
                'Select a Forecastseries from '
                f"{forecasts[0]['starttime']} to "
                f"{forecasts[-1]['endtime']}"
            ], style={'margin': '10px auto'}),
            html.Div([
                dcc.Slider(
                    start, end,
                    step=None,
                    marks=marks,
                    value=None,
                    id='slider-fc-ps',
                )
            ],
                style={'width': '90%', 'marginBottom': '30px'})
        ])], [html.Div([
            html.Div([
                dmc.Select(
                    label='Select Injection Plan',
                    value=initial["value"],
                    placeholder=initial["label"],
                    id='select-in-ps',
                    data=data_select,
                    style={'width': 200, 'marginBottom': 10},
                ),
            ],
                style={'width': '45%', 'marginBottom': '10px'})
        ])], [html.Div([
            html.Div([
                dmc.Select(
                    label='Select Plotting Style',
                    value=data_radio[0]["value"],
                    placeholder=data_radio[0]["label"],
                    id='cum-rates',
                    data=data_radio,
                    style={'width': 200, 'marginBottom': 10},
                ),
            ],
                style={'width': '45%', 'marginBottom': '10px'})
        ])], uuid_map

    @callback(Output({'type': 'select-fs-ps', 'index': MATCH}, 'data'),
              Input({'type': 'select-fs-ps', 'index': MATCH}, 'value'))
    def get_forecastseries(input):
        client = HermesClient(url=RAMSIS_URL)

        # TODO: the next line takes all forecastseries in the database,
        # regardless of the project id.
        forecastseries = client.list_forecastseries(project_id=PROJECT_ID)
        forecastseries = sorted(
            forecastseries, key=lambda x: x['id'], reverse=True)
        data_slider = [{'value': fc['id'], 'label': fc['name']}
                       for fc in forecastseries]
        return data_slider
