# Color palette for plotly figures
TLS_COLORS = ['#95BF8F', '#E3B678', '#D57272']  # green yellow red
DARK_TLS_COLORS = ['#41683C', '#A97123', '#8D2A2A']  # green yellow red
MODEL_COLORS = ['#719DAD', '#967387']  # blue pink
SHADES_MODEL_1 = ['#34505B', '#527E8E', '#719DAD',
                  '#97B8C3', '#BED3DA']  # from darker to lighter
SHADES_MODEL_2 = ['#513D48', '#745868', '#967387',
                  '#B096A5', '#CAB9C3']  # from darker to lighter


def figure_styling(fig, str_xaxis, str_yaxis, row, col, str_yaxis2=None,
                   secondary_y=False):
    '''
    Styling figures with same background color, font family and size
    Args:
        fig: figure to be styled
        str_xaxis: x-axis title
        str_yaxis: y-axis title
        row: row in the subplot
        col: column in the subplot
        str_yaxis2: y-axis title for secondary y-axis
        secondary_y: boolean for secondary y-axis
    Returns:
        fig: styled figure
    '''
    fig.update_xaxes(title_text=str_xaxis,
                     ticks='outside',
                     showline=True,
                     linecolor='black',
                     gridcolor='lightgrey', row=row, col=col)

    if secondary_y:
        fig.update_yaxes(title_text=str_yaxis,
                         ticks='outside',
                         showline=True,
                         linecolor='black',
                         gridcolor='lightgrey', type="log",
                         row=row, col=col, secondary_y=False)
        fig.update_yaxes(title_text=str_yaxis2,
                         ticks='outside',
                         showline=True,
                         linecolor='black',
                         gridcolor='lightgrey', type="log",
                         row=row, col=col, secondary_y=True)
    else:
        fig.update_yaxes(title_text=str_yaxis,
                         ticks='outside',
                         showline=True,
                         linecolor='black',
                         gridcolor='lightgrey', type="log",
                         row=row, col=col)

    fig.update_layout(
        plot_bgcolor='white',
        font_family="Verdana",
        font_size=14)

    return fig


def basic_style(fig):
    fig.update_layout(
        plot_bgcolor='white',
        font_family="Verdana",
        font_size=14)
    fig.update_xaxes(ticks='outside',
                     showline=True,
                     linecolor='black',
                     gridcolor='lightgrey')
    fig.update_yaxes(ticks='outside',
                     showline=True,
                     linecolor='black',
                     gridcolor='lightgrey')
    return fig


def basic_style_2yaxes(fig):
    fig.update_layout(
        plot_bgcolor='white',
        font_family="Verdana",
        font_size=14)
    fig.update_xaxes(ticks='outside',
                     showline=True,
                     linecolor='black',
                     gridcolor='lightgrey')
    fig.update_yaxes(ticks='outside',
                     showline=True,
                     linecolor='black',
                     gridcolor='lightgrey',
                     secondary_y=False)
    fig.update_yaxes(ticks='outside',
                     showline=True,
                     linecolor='black',
                     gridcolor='lightgrey',
                     secondary_y=True)

    return fig
