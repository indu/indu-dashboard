import os

from dotenv import load_dotenv

load_dotenv()

HYDWS_URL = os.getenv('HYDWS_URL')
RAMSIS_URL = os.getenv('RAMSIS_URL')
FDSN_URL = os.getenv('FDSN_URL')
FDSN_URL_EXP = os.getenv('FDSN_URL_EXP')

FDSN_FBG_URL = os.getenv('FDSN_FBG_URL')
FDSN_FBG_USER = os.getenv('FDSN_FBG_USER')
FDSN_FBG_PASSWD = os.getenv('FDSN_FBG_PASSWD')

INFLUXDB_URL = os.getenv('INFLUXDB_URL')
INFLUXDB_TOKEN = os.getenv('INFLUXDB_TOKEN')

FBG_BOREHOLES_DEPTHS = {"BFE18":
                        {"depth": {"F18Z1": 5.8,
                                   "F18Y1": 7.9,
                                   "F18Z2": 10.3,
                                   "F18Y2": 13.5,
                                   "F18Z3": 14.6,
                                   "F18Y3": 16.97,
                                   "F18Z4": 18.7,
                                   "F18Y4": 20.32,
                                   "F18Z5": 23.9,
                                   "F18Y5": 25.2,
                                   "F18Z6": 27.4,
                                   "F18Y6": 28.66,
                                   "F18Z7": 30.8,
                                   "F18Y7": 33.8,
                                   "F18Z8": 35.6,
                                   "F18Y8": 39.6,
                                   }
                         },
                        "BPR12":
                        {"depth": {"P12Z1": 0.19,
                                   "P12Y1": 0.315,  # 0.375,
                                   "P12Z6": 0.44,  # 0.5,
                                   "P12Y6": 0.565,  # 0.625,
                                   "P12X1": 0.69,  # 0.75,
                                   "P12X4": 0.815,  # 0.875,
                                   "P12Z2": 0.94,  # 1.0,
                                   "P12Y2": 1.065,  # 1.125,
                                   "P12Z5": 1.19,  # 1.25,
                                   "P12Y5": 1.315,  # 1.375,
                                   "P12X2": 1.815,  # 1.75,
                                   "P12Z3": 2.165,  # 2.25,
                                   "P12Y3": 2.815,  # 2.75,
                                   "P12X3": 3.065,  # 3.15,
                                   "P12Z4": 3.165,  # 3.30,
                                   "P12Y4": 3.815,  # 3.75,
                                   }
                         }
                        }

SIMFIP_BOREHOLES = {"BFE05":
                    {"bucket": "tests_here",
                     "channels": {'FNorm': 'Fault Opening',
                                  'FslipMag': 'Fault Slip',
                                  'FslipN': 'Disp. North',
                                  'FslipE': 'Disp. East',
                                  'FslipU': 'Disp. Up'}
                     },
                    "BFE06":
                    {"bucket": "tests_here",
                     "channels": {'FNorm_2': 'Fault Opening',
                                  'FslipMag_2': 'Fault Slip',
                                  'FslipN_2': 'Disp. North',
                                  'FslipE_2': 'Disp. East',
                                  'FslipU_2': 'Disp. Up'}
                     },
                    "BFE07":
                    {"bucket": "8R",
                     "channels": {'BX': 'X',
                                  'BY': 'Y',
                                  'BZ': 'Z'}
                     },
                    "Tunnel Wall":
                    {"bucket": "8R",
                     "channels": {'WX': 'X',
                                  'WY': 'Y',
                                  'WZ': 'Z'}
                     }

                    }

INJ_BOREHOLE = 'BFE05'
INJ_SECTION = 'BFE05_section_01'
# BOREHOLES_SET1 = ['BFE05','BFE06','BFE07','BFE18']

BOREHOLES_SET1 = {
    "BFE05": {
        "bucket": "tests_here",
        "channels": {
            '1_P2': {'description': 'Pressure Zone',
                     'scaling': 0.1},
            'injection_A_MPa': {'description': 'Injection Pressure',
                                'scaling': 1},
            'flow_pumps_A_l_min': {'description': 'Injection rate',
                                   'scaling': 1},
        }
    },
    "BFE06": {
        "bucket": "tests_here",
        "channels": {
            '2_P2': {'description': 'Pressure Zone',
                     'scaling': 0.1},
            'injection_B_MPa': {'description': 'Injection Pressure',
                                'scaling': 1},
            'flow_pumps_B_l_min': {'description': 'Injection rate',
                                   'scaling': 1},
        }
    },
    "BFE07": {
        "bucket": "tests_here",
        "channels": {
            'BFE07_uphole_bar': {'description': 'Pressure Zone',
                                 'scaling': 0.1},
        }
    },
    "BFE18": {
        "bucket": "tests_here",
        "channels": {
            '1-1_bar': {'description': '1-1', 'scaling': 0.1},
            '1-2_bar': {'description': '1-2', 'scaling': 0.1},
            '1-3_bar': {'description': '1-3', 'scaling': 0.1},
            '2-1_bar': {'description': '2-1', 'scaling': 0.1},
            '2-2_bar': {'description': '2-2', 'scaling': 0.1},
            '2-3_bar': {'description': '2-3', 'scaling': 0.1},
            '3-1_bar': {'description': '3-1', 'scaling': 0.1},
            '3-2_bar': {'description': '3-2', 'scaling': 0.1},
            '3-3_bar': {'description': '3-3', 'scaling': 0.1},
        }
    }
}

BOREHOLES_SET2 = ['ST1', 'MB2', 'MB5', 'MB8']

PROJ_STRING = 'epsg:2056'
REF_E, REF_N, REF_A = (2679720.696, 1151600.128, 1485)

PROJECT_ID = 'e667b5b0-1e8e-487c-92fa-30f3154f7837'
# Model variables
MODEL_1_NAME = 'em1'
ALTERNATIVE_MODEL_1_NAME = 'em1'
MODEL_2_NAME = 'ml1'
MODEL_3_NAME = 'hm1d'
NUMBER_PARAM_MODEL_1 = 2
NUMBER_PARAM_MODEL_2 = 7
NUMBER_PARAM = {MODEL_1_NAME: 2,
                MODEL_2_NAME: 7}
MODELS = [MODEL_1_NAME, MODEL_2_NAME, MODEL_3_NAME]

# Catalogue & seismicity variables
SEISMOSTATS_MAGNITUDE_COLUMN = 'magnitude'
SEISMOSTATS_TIME_COLUMN = 'time'
SEISMOSTATS_PHASE_COLUMN = 'associatedphasecount'
MIN_LON = 8.45
MAX_LON = 8.50
MIN_LAT = 46.49
MAX_LAT = 46.53

BINNING = 0.01
MAG_ORANGE_TLS = -0.41
MAG_RED_TLS = 0.0
MAGNITUDE_COLUMN = 'magnitude'
TIME_COLUMN = 'isotime'
NUM_PHASES_COLUMN = 'num_phase'
MIN_NUM_PHASES = 3
