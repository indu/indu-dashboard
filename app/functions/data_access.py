from datetime import datetime

import numpy as np
import pandas as pd
from hermes_client.clients import ForecastSeriesClient

from app.functions.global_variables import MODELS  # noqa


def fetch_forecast_modelruns(fc_client: ForecastSeriesClient,
                             forecast_id: int | None,
                             injectionplans: list[str] | None = None,
                             models: list[str] | None = None):

    forecast_info = next(f for f in fc_client.list_forecasts_info()
                         if f['id'] == forecast_id)

    forecast_start = datetime.strptime(
        forecast_info['starttime'], '%Y-%m-%dT%H:%M:%S')

    # get all forecasts, filtered by injectionplans and models
    modelruns = fc_client.list_forecast_rates(
        forecast_id=forecast_id,
        modelconfigs=models,
        injectionplans=injectionplans
    )

    # get one modelrun for each modelconfig
    # since fit is the same per modelconfig
    fit_tbl = {}
    # TODO: apparently when adding an extra model this is saved
    # also for past forecastseries and the dashboard fails.
    # I have added now a filter based on the names of the
    # models in the global_variables.py.

    for model in fc_client.modelconfigs:
        m = next((fc for fc in modelruns if
                 fc['modelconfig_name'] == model['name']), None)
        if m:
            fit_tbl[model['name']] = m

    # get fit statistics for each modelconfig
    for mdl, fc in fit_tbl.items():
        fit = [fit for fit in fc['rateforecasts']
               if fit.starttime < forecast_start]
        fit_tbl[mdl] = get_rates_statistics(fit)

    # add modelconfig, injectionplan, hydraulics,
    # fit and seismicity to forecasts
    for idx, fc in enumerate(modelruns):
        modelruns[idx]['rates_fit'] = fit_tbl[fc['modelconfig_name']]

    # get statistics for modelruns
    for idx, rates in enumerate(modelruns):
        fc = [step for step in rates['rateforecasts']
              if step.starttime >= forecast_start]
        modelruns[idx]['rates_forecast'] = get_rates_statistics(fc)

    return modelruns


def get_latest_forecast(fc_client: ForecastSeriesClient):
    return max(fc_client.list_forecasts_info(),
               key=lambda x: datetime.strptime(
                   x['starttime'], '%Y-%m-%dT%H:%M:%S')
               if x['status'] == 'COMPLETED' else datetime(1900, 1, 1))


def models_tbl(fc_client: ForecastSeriesClient):
    # lookuptable for modelconfigs
    return {mc['name']: mc for mc in
            fc_client.list_modelconfigs()}


def injectionplans_tbl(fc_client: ForecastSeriesClient):
    # lookuptable for injectionplans
    return {ip['name']: ip for ip in
            fc_client.list_injectionplans()}


def std(x): return np.std(x)


def get_rates_statistics(forecasts: list):

    forecast = pd.concat([mdl.add_time_index() for mdl in forecasts])

    forecast['a_std'] = forecast['a']
    forecast['number_events_std'] = forecast['number_events']
    forecast['b_std'] = forecast['b']
    forecast['mc_std'] = forecast['mc']
    forecast['alpha_std'] = forecast['alpha']

    statistics = forecast.groupby(level=[0, 1]).agg(
        {'number_events': 'median',
         'b': 'median',
         'mc': 'median',
         'a': 'median',
         'alpha': 'median',
         'number_events_std': std,
         'b_std': std,
         'mc_std': std,
         'a_std': std,
         'alpha_std': std, })

    statistics.reset_index(inplace=True)

    statistics.index = \
        statistics.apply(lambda x: x['starttime'] +
                         (x['endtime'] - x['starttime']) / 2, axis=1)

    statistics.drop(columns=['starttime', 'endtime'], inplace=True)

    return statistics
