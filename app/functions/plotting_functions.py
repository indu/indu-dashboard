import json

import numpy as np
import pandas as pd
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots
from plotly.validators.scatter.line import DashValidator
from plotly.colors import qualitative

from app.functions.data_processing import seismicity_rate_model_fit
from app.functions.figure_styling import (DARK_TLS_COLORS, MODEL_COLORS,
                                          SHADES_MODEL_1, SHADES_MODEL_2,
                                          TLS_COLORS, basic_style,
                                          basic_style_2yaxes)
from app.functions.global_variables import (ALTERNATIVE_MODEL_1_NAME,
                                            MAG_ORANGE_TLS, MAG_RED_TLS,
                                            MAGNITUDE_COLUMN, MIN_NUM_PHASES,
                                            MODEL_1_NAME, MODEL_2_NAME,
                                            TIME_COLUMN)


def plot_b_value_space(cat, scaler, bvalue_space_300, key):

    if len(cat) == 0:
        cat['X'] = np.nan
        cat['Y'] = np.nan
        cat['Z'] = np.nan
        cat['seconds'] = np.nan
        cat['hover_text'] = None
        bvalue_space_300[key] = np.nan

    event_trace = go.Scatter3d(
        x=cat['x'],
        y=cat['y'],
        z=cat['z'],
        mode='markers',
        marker=dict(
            size=cat[MAGNITUDE_COLUMN].apply(lambda x: mag_to_size(scaler, x)),
            color=bvalue_space_300[key],  # 'rgb(17, 157, 255)',
            opacity=0.5,
            symbol='circle',
            cmax=2.0,
            cmin=0.7,
            colorbar=dict(

                # tickmode = "array",
                # tickvals = tick,
                # ticktext = tick_label,
                # orientation = "h"
                x=-0.3,
                ticklabelposition="outside right"
            ),
            colorscale="magma",
            # line=dict(color='rgb(17, 157, 255)', width=0.1)
        ),
        name='events',
        hoverinfo='text',
        text=cat['hover_text'],
    )

    scatter3d_fig = go.Figure()

    scatter3d_fig.add_trace(event_trace)
    camera = dict(
        up=dict(x=0, y=0, z=1),
        center=dict(x=0, y=0, z=0),
        eye=dict(x=-1, y=-2, z=1.25)
    )
    scatter3d_fig.update_layout(autosize=True,
                                height=1200,
                                font=dict(
                                    family='Verdana',
                                    size=14,
                                ),
                                scene={'aspectmode': 'cube'},
                                scene_camera=camera,
                                )

    scatter3d_fig.update_scenes(xaxis_title_text='Easting [m]',
                                yaxis_title_text='Northing [m]',
                                zaxis_title_text='Altitude [m]', )

    return scatter3d_fig


def plotting_hydraulics(section_hydraulics, title='title', injection_point=False):
    fig = make_subplots(specs=[[{'secondary_y': True}]])

    if section_hydraulics == None:
        return fig

    linestyles = DashValidator().values
    linecolors = qualitative.T10

    for idx, sec in enumerate(section_hydraulics):
        if not sec.hydraulics.empty:
            #if injection_point:
                if 'topflow' in sec.hydraulics.columns:
                    fig.add_trace(
                        go.Scatter(x=sec.hydraulics.index,
                                   y=sec.hydraulics['topflow'] * 60000,
                                   mode='lines',
                                   line=dict(color=linecolors[
                                                 idx % (len(linecolors) - 1)],
                                             dash=linestyles[
                                                 idx % (len(linestyles) - 1)]),
                                   name=f"Flow {sec.metadata['name']}"),secondary_y=True)
                    fig.update_yaxes(title_text='<b>Flow [l/min]</b>', secondary_y=True)
                if 'toppressure' in sec.hydraulics.columns:
                    fig.add_trace(
                        go.Scatter(x=sec.hydraulics.index,
                                   y=sec.hydraulics['toppressure'] / 1.e6,
                                   mode='lines',
                                   line=dict(color=linecolors[
                                                 idx+1 % (len(linecolors) - 1)],
                                             dash=linestyles[
                                                 idx % (len(linestyles) - 1)]),
                                   name=f"Pressure {sec.metadata['name']}"),
                        secondary_y=False)
                    fig.update_yaxes(title_text='<b>Pressure [MPa]</b>', secondary_y=False)
                if 'bottompressure' in sec.hydraulics.columns:
                    fig.add_trace(
                        go.Scatter(x=sec.hydraulics.index,
                                   y=sec.hydraulics['bottompressure'] / 1.e6,
                                   mode='lines',
                                   line=dict(color=linecolors[
                                                 idx+2 % (len(linecolors) - 1)],
                                             dash=linestyles[
                                                 idx % (len(linestyles) - 1)]),
                                   name=f"Zone Pressure {sec.metadata['name']}"),
                        secondary_y=False)
                    fig.update_yaxes(title_text='<b>Pressure [MPa]</b>', secondary_y=False)

    #            else:
#                if 'toppressure' in sec.hydraulics.columns:
#                    fig.add_trace(
#                        go.Scatter(x=sec.hydraulics.index,
#                                   y=sec.hydraulics['toppressure'] / 1.e6,
#                                   mode='lines',
#                                   line=dict(color=linecolors[
#                                                 idx % (len(linecolors) - 1)],
#                                             dash=linestyles[
#                                                 idx % (len(linestyles) - 1)]),
#                                   name=f"{sec.metadata['name']}"),
#                        secondary_y=False)
#                fig.update_yaxes(title_text='<b>Pressure [MPa]</b>', secondary_y=False, )


    # Set y-axes titles
    fig.update_xaxes(title_text='Time [UTC]')
    fig.update_legends({'x': 0, 'y': 1, 'yanchor': 'top'})
    fig.update_layout(yaxis={'showexponent': 'all', 'exponentformat': 'e'},
                      yaxis2={'showexponent': 'all', 'exponentformat': 'e'})

    basic_style_2yaxes(fig)
    fig.update_layout(
        title=dict(text=title)
    )

    return fig


def mag_to_size(scaler, moment_magnitude):
    '''
    converts magnitude to size for plotting
    Args:
        scaler: scaling factor
        moment_magnitude: moment magnitude of the events
    Returns:
        resized_magnitude_for_plotting: size of the events rezised for plotting
    '''
    # [Pa] stress drop assumption, could also be higher than 1MPa
    sigma = 1e6
    # [Nm] Scalar seismic moment from magnitude (Kanamori)
    scalar_seismic_moment = 10**((moment_magnitude + 6.03) * 3 / 2)
    # [m] Source radii from seismic moment & stress drop (Keilis-Borok, 1959)
    source_radius = (scalar_seismic_moment * 7 / 16 * 1 / sigma)**(1 / 3)
    # scale the physical unit by something sensible for the plot
    resized_magnitude_for_plotting = source_radius * scaler
    # limit the size range
    resized_magnitude_for_plotting = max(
        min(resized_magnitude_for_plotting, 30), 1)
    return resized_magnitude_for_plotting


def load_boreholes(bh_url):
    with open(bh_url, 'r') as file:
        boreholes_data = json.load(file)
    return boreholes_data


def build_borehole_traces(boreholes):
    traces = []
    for key, bhdata in boreholes.items():
        xx = np.array(bhdata['Easting'])
        yy = np.array(bhdata['Northing'])
        zz = np.array(bhdata['Z'])
        xx = xx[zz <= 20]
        yy = yy[zz <= 20]
        zz = zz[zz <= 20]
        if key == 'tunnel':
            continue
        borehole_trace = {
            'type': 'scatter3d',
            'mode': 'lines',
            'x': xx,
            'y': yy,
            'z': zz,
            'name': key,
            'hovertemplate': key,
            'opacity': 0.5,
            'line': {
                'color': 'rgb(127, 127, 127)',
                'width': 7
            },
            # 'visible': 'legendonly'
        }
        traces.append(borehole_trace)
    return traces


def plot_LLe(LLe_model_1, LLe_model_2, model1_name, model2_name):
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=LLe_model_1.index,
                             y=LLe_model_1['log_likelihood'],
                             mode='lines',
                             line=dict(
                                 color=MODEL_COLORS[0]), name='LLe '+model1_name))
    fig.add_trace(go.Scatter(x=LLe_model_2.index,
                             y=LLe_model_2['log_likelihood'],
                             mode='lines',
                             line=dict(
                                 color=MODEL_COLORS[1]), name='LLe '+model2_name))
    fig.update_layout(title='Log-likelihood on the fit period',
                      xaxis_title='Time',
                      yaxis_title='Log-likelihood')
    basic_style(fig)
    return fig


def plot_cLLe(LLe_model_1, LLe_model_2, model1_name, model2_name):
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=LLe_model_1.index,
                             y=LLe_model_1['cumulative_log_likelihood'],
                             mode='lines',
                             line=dict(
                                 color=MODEL_COLORS[0]), name='LLe '+model1_name))
    fig.add_trace(go.Scatter(x=LLe_model_2.index,
                             y=LLe_model_2['cumulative_log_likelihood'],
                             mode='lines',
                             line=dict(
                                 color=MODEL_COLORS[1]), name='LLe '+model2_name))
    fig.update_layout(title='Cumulative log-likelihood on the fit period',
                      xaxis_title='Time',
                      yaxis_title='Log-likelihood')
    basic_style(fig)
    return fig


def plot_IG(InfoGain):
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=InfoGain.index,
                             y=InfoGain['information_gain'],
                             mode='lines',
                             line=dict(
                                 color=TLS_COLORS[0]),
                             name='Information Gain'))
    fig.update_layout(title='Information Gain on the fit period',
                      xaxis_title='Time',
                      yaxis_title='Information Gain')
    basic_style(fig)
    return fig


def plot_fit_rate(fig, fit_model, model_name, cum=False):

    if model_name == MODEL_1_NAME or model_name == ALTERNATIVE_MODEL_1_NAME:
        color = MODEL_COLORS[0]
    elif model_name == MODEL_2_NAME:
        color = MODEL_COLORS[1]
    else:  # default color
        color = DARK_TLS_COLORS[0]

    if cum:
        YY = np.cumsum(fit_model['number_events'].values)
    else:
        YY = fit_model['number_events'].values

    fig.add_trace(go.Scatter(x=fit_model.index,
                             y=YY,
                             mode='lines',
                             line=dict(
                                 color=color),
                             name=model_name),
                  secondary_y=False)
    return fig


def plot_seismicity_rate(fig, model_fit, model_seismicity, cum=False):
    n_events_mc = seismicity_rate_model_fit(model_fit, model_seismicity, cum)
    fig.add_trace(go.Scatter(x=n_events_mc.index,
                             y=n_events_mc['number_events'],
                             mode='lines',
                             line=dict(
                                 color='#333333'),
                             name='Recorded seismcity'),
                  secondary_y=False)
    return fig


def add_forecast_rates_plot(fig, model_forecast, model_name, cum=False):
    if model_name == MODEL_1_NAME or model_name == ALTERNATIVE_MODEL_1_NAME:
        color = MODEL_COLORS[0]
        shade_color = SHADES_MODEL_1[4]
    elif model_name == MODEL_2_NAME:
        color = MODEL_COLORS[1]
        shade_color = SHADES_MODEL_2[4]
    else:  # default color
        color = DARK_TLS_COLORS[0]
        shade_color = TLS_COLORS[0]
    if cum:
        model_forecast_std_rate_plus = np.cumsum(model_forecast.number_events +
                                                 model_forecast.number_events_std)
        model_forecast_std_rate_minus = np.cumsum(model_forecast.number_events -
                                                  model_forecast.number_events_std)
    else:
        model_forecast_std_rate_plus = model_forecast.number_events + \
            model_forecast.number_events_std
        model_forecast_std_rate_minus = model_forecast.number_events - \
            model_forecast.number_events_std

    fig.add_trace(go.Scatter(x=np.concatenate((model_forecast.index,
                                               model_forecast.index[::-1])),
                             y=np.concatenate(
                                 (model_forecast_std_rate_plus,
                                  model_forecast_std_rate_minus[::-1])),
                             fill='toself',
                             fillcolor=shade_color,
                             line=dict(color=shade_color),
                             mode="lines",
                             legendgroup='Forecasted rate ' + model_name,
                             hoverinfo="skip", showlegend=False),
                  secondary_y=False)

    if cum:
        YY = np.cumsum(model_forecast.number_events)
    else:
        YY = model_forecast.number_events
    fig.add_trace(go.Scatter(x=model_forecast.index,
                             y=YY,
                             mode='lines',
                             legendgroup='Forecasted rate ' + model_name,
                             name='Forecasted rate ' + model_name,
                             line=dict(dash='dash', color=color)),
                  secondary_y=False)

    return fig


def plot_flow_rate(fig, hydraulics_df, plan=False, timeindex=[None, None]):
    if plan:
        # filter hydraulics to keep only after the timeindex
        hydraulics_df = hydraulics_df[hydraulics_df.index >= timeindex[0]]
        hydraulics_df = hydraulics_df[hydraulics_df.index <= timeindex[1]]
        fig.add_trace(go.Scatter(x=hydraulics_df.index,
                                 y=hydraulics_df['topflow']*60000,
                                 mode='lines',
                                 line=dict(dash='dash',
                                           color=TLS_COLORS[1]),
                                 name='Planned flow rate (L/min)',
                                 legendgroup="flow", showlegend=False),
                      secondary_y=True)
    else:
        fig.add_trace(go.Scatter(x=hydraulics_df.index,
                                 y=hydraulics_df['topflow']*60000,
                                 mode='lines',
                                 line=dict(
                                     color=TLS_COLORS[1]),
                                 name='Flow rate (L/min)',
                                 legendgroup="flow"),
                      secondary_y=True)

    fig.update_yaxes(title_text='Flow rate (L/min)',
                     secondary_y=True, tickmode="sync",)
    return fig


def plot_forecast_rates(model_seismicity, model_hydraulics, models_name, models_fit, plan_df, models_forecast, cum=False):

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    fig = plot_flow_rate(fig, model_hydraulics)

    injection_plan = plan_df.resample('60s').interpolate()
    injection_plan = injection_plan[injection_plan.index >=
                                    model_hydraulics.index[-1]]

    sc_all_negative = (injection_plan['topflow'] < 0).all()
    sc_all_identical = injection_plan['topflow'].nunique() == 1

    if sc_all_negative and sc_all_identical:
        injection_plan["topflow"] = model_hydraulics['topflow'].values[-1] * \
            (-injection_plan["topflow"])

    fig = plot_flow_rate(fig, injection_plan, plan=True,
                         timeindex=[models_fit[0].index[-1], models_forecast[0].index[-1]])

    fig = plot_seismicity_rate(fig, models_fit[0], model_seismicity, cum)

    for model, name in zip(models_fit, models_name):
        fig = plot_fit_rate(fig, model, name, cum)

    if cum:
        for fit, forecast in zip(models_fit, models_forecast):
            forecast['number_events'].values[0] = forecast['number_events'].values[0] + \
                np.cumsum(fit['number_events']).values[-1]

    for model, name in zip(models_forecast, models_name):
        fig = add_forecast_rates_plot(fig, model, name, cum)

    # vertical dashed line at end of fit
    fig.add_vline(x=model_hydraulics.index[-1],
                  line_dash="dash", line_color="#777777")
    # y-axis 1 label 'Seismicity rate'
    if cum:
        fig.update_yaxes(title_text="Num. events", secondary_y=False)
        fig.update_layout(title='Forecasted number of events', showlegend=True)
    else:
        fig.update_yaxes(title_text="Rate", secondary_y=False)
        fig.update_layout(
            title='Forecasted rate of seismicity', showlegend=True)

    basic_style_2yaxes(fig)

    fig.update_legends({'x': 1.05, 'y': 1, 'xanchor': 'left'})

    return fig


def add_trace_FMD_forecast(fig, GR_y, model_name, error=False):
    if model_name == MODEL_1_NAME or model_name == ALTERNATIVE_MODEL_1_NAME:
        color = MODEL_COLORS[0]
        shade_color = SHADES_MODEL_1[4]
    elif model_name == MODEL_2_NAME:
        color = MODEL_COLORS[1]
        shade_color = SHADES_MODEL_2[4]
    else:  # default color
        color = DARK_TLS_COLORS[0]
        shade_color = TLS_COLORS[0]

    if error:
        fig.add_trace(go.Scatter(x=np.concatenate((GR_y.index,
                                                   GR_y.index[::-1])),
                                 y=np.concatenate(
                                     (GR_y['GR_y_plus'].values,
                                      GR_y['GR_y_minus'].values[::-1])),
                                 fill='toself',
                                 fillcolor=shade_color,
                                 line=dict(color=shade_color),
                                 mode="lines",
                                 legendgroup='Forecast ' + model_name,
                                 hoverinfo="skip", showlegend=False))

    fig.add_trace(go.Scatter(x=GR_y.index,
                             y=GR_y['GR_y'],
                             mode='lines', line=dict(
                                 color=color),
                             legendgroup='Forecast ' + model_name,
                             name='Forecast ' + model_name))

    return fig


def add_trace_FMD_fit(fig, GR_y, model_name):
    if model_name == MODEL_1_NAME or model_name == ALTERNATIVE_MODEL_1_NAME:
        color = MODEL_COLORS[0]
    elif model_name == MODEL_2_NAME:
        color = MODEL_COLORS[1]
    else:  # default color
        color = DARK_TLS_COLORS[0]

    string_stats_catalogue = 'Mc = ' + str(round(GR_y['mc'].mean(), 2)) + ', b = ' + \
        str(round(GR_y['b'].mean(), 2)) + ' \u00B1 ' + \
        str(round(GR_y['b_std'].mean(), 3))
    fig.add_trace(go.Scatter(x=GR_y.index,
                             y=GR_y['GR_y_fit'],
                             mode='lines', line=dict(dash='dash',
                                                     color=color),
                             name='Fit ' + model_name + ' ' + string_stats_catalogue))

    return fig


def plot_FMD_forecast(GR_models, modelnames, model_seismicity):
    fig = go.Figure()

    model_seismicity = model_seismicity[model_seismicity['magnitude'].notna()]
    # filter seismicity to remove events with less than MIN_NUM_PHASES
    if 'associatedphasecount' in model_seismicity.columns:
        model_seismicity = model_seismicity[model_seismicity[
            'associatedphasecount'] >= MIN_NUM_PHASES]

    HH, bins = np.histogram(
        model_seismicity['magnitude'], bins=GR_models[0].index)
    BinC = bins[:-1] + np.diff(bins) / 2

    fig.add_trace(go.Scatter(x=BinC,
                             y=np.cumsum(HH[::-1])[::-1],
                             mode='markers',
                             marker=dict(symbol='circle', size=10,
                                         color=TLS_COLORS[0],
                                         line=dict(width=2,
                                                   color=DARK_TLS_COLORS[0])),
                             name='data' + ', cumulative'))


    for GR, name in zip(GR_models, modelnames):
        add_trace_FMD_forecast(fig, GR, name, error=True)
        add_trace_FMD_fit(fig, GR, name)

    fig.update_layout(title='Forecasted FMD',
                      xaxis_title='Magnitude',
                      yaxis_title='Number of events')

    max_events = max([max(gr['GR_y_plus']) for gr in GR_models])+100

    # set y-axis as log
    fig.update_yaxes(type="log", exponentformat='e', showexponent='all', range=[
                     np.log10(0.8), np.log10(max_events)])

    fig.update_yaxes()

    basic_style(fig)

    return fig


def add_trace_Pocc_forecast(fig, Pocc_df, model_name, cumulative=False):
    if model_name == MODEL_1_NAME or model_name == ALTERNATIVE_MODEL_1_NAME:
        color = MODEL_COLORS[0]
        color_fill = SHADES_MODEL_1[4]
    elif model_name == MODEL_2_NAME:
        color = MODEL_COLORS[1]
        color_fill = SHADES_MODEL_2[4]
    else:  # default color
        color = DARK_TLS_COLORS[0]
        color_fill = TLS_COLORS[0]
    # if cumulative:
    #    Pocc_df['Pocc'] = Pocc_df['Pocc'].cumsum()
    fig.add_trace(go.Scatter(x=Pocc_df.index, y=Pocc_df['Pocc'], mode='lines',
                             line=dict(color=color), name=model_name, legendgroup=model_name))
    fig.add_trace(go.Scatter(x=np.concatenate((Pocc_df.index,
                                               Pocc_df.index[::-1])),
                             y=np.concatenate(
        (Pocc_df['Pocc_plus'],
         Pocc_df['Pocc_minus'][::-1])),
        fill='toself',
        fillcolor=color_fill,
        line=dict(color=color_fill),
        mode="lines",
        hoverinfo="skip",
        showlegend=False,
        opacity=0.5, legendgroup=model_name))

    return fig


def plot_Pocc_forecast(Pocc, color, model_name, cumulative=False):
    fig = go.Figure()

    if color == 'orange':
        for Pocc_df, name in zip(Pocc, model_name):
            df_Pocc = pd.DataFrame(columns=['Pocc', 'Pocc_plus', 'Pocc_minus'])
            df_Pocc['Pocc'] = Pocc_df['Pr_orange']
            df_Pocc['Pocc_plus'] = Pocc_df['Pr_orange_plus']
            df_Pocc['Pocc_minus'] = Pocc_df['Pr_orange_minus']
            mag_y_label = MAG_ORANGE_TLS
            color_code = DARK_TLS_COLORS[1]
            add_trace_Pocc_forecast(fig, df_Pocc, name, cumulative)
    elif color == 'red':
        for Pocc_df, name in zip(Pocc, model_name):
            df_Pocc = pd.DataFrame(
                columns=['Pocc_red', 'Pocc_plus', 'Pocc_minus'])
            df_Pocc['Pocc'] = Pocc_df['Pr_red']
            df_Pocc['Pocc_plus'] = Pocc_df['Pr_red_plus']
            df_Pocc['Pocc_minus'] = Pocc_df['Pr_red_minus']
            mag_y_label = MAG_RED_TLS
            color_code = DARK_TLS_COLORS[2]
            add_trace_Pocc_forecast(fig, df_Pocc, name, cumulative)
    else:
        raise ValueError('color should be orange or red')

    fig.update_layout(title='Forecasted probability of exceedance for an '
                      + color + ' alert',
                      xaxis_title='Time',
                      yaxis_title='Probability M \u2265 ' +
                      str(mag_y_label),
                      yaxis_color=color_code,
                      showlegend=True)

    basic_style(fig)

    return fig


def add_catalogue_FMD_plot(fig, catalogue_dict, colour_catalogue, catalogue_name):
    '''
    Add a catalogue FMD plot to a figure
    Args:
        fig: plotly figure object
        catalogue_dict: dictionary with catalogue statistical data
        colour_catalogue: colour of the catalogue
        catalogue_name: name of the catalogue
    Returns:
        fig: plotly figure object with added traces for non-cumulative and
            cumulative FMD, and GR fit
    '''
    if colour_catalogue == 'green':
        colour_plot = DARK_TLS_COLORS[0]
        colour_fill = TLS_COLORS[0]
    elif colour_catalogue == 'orange':
        colour_plot = DARK_TLS_COLORS[1]
        colour_fill = TLS_COLORS[1]
    elif colour_catalogue == 'red':
        colour_plot = DARK_TLS_COLORS[2]
        colour_fill = TLS_COLORS[2]
    elif colour_catalogue == 'blue':
        colour_plot = SHADES_MODEL_1[2]
        colour_fill = SHADES_MODEL_1[4]
    elif colour_catalogue == 'pink':
        colour_plot = SHADES_MODEL_2[2]
        colour_fill = SHADES_MODEL_2[4]
    else:
        colour_plot = 'black'
        colour_fill = 'grey'

    # Non cumulative FMD
    fig.add_trace(go.Scatter(x=catalogue_dict['catalogue_bins'],
                             y=catalogue_dict['catalogue_count'],
                             mode='markers',
                             marker=dict(symbol='square', size=10,
                                         color=colour_fill,
                                         line=dict(width=2,
                                                   color=colour_plot)),
                             name=catalogue_name + ', non cumulative'))
    # Cumulative FMD
    fig.add_trace(go.Scatter(x=catalogue_dict['catalogue_cum_bins'],
                             y=catalogue_dict['catalogue_cum_count'],
                             mode='markers',
                             marker=dict(symbol='circle', size=10,
                                         color=colour_fill,
                                         line=dict(width=2,
                                                   color=colour_plot)),
                             name=catalogue_name + ', cumulative'))
    # GR fit
    fig.add_trace(go.Scatter(x=catalogue_dict['catalogue_cum_bins'][
        catalogue_dict['catalogue_cum_bins'] >= catalogue_dict['catalogue_mc']],
        y=catalogue_dict['GR_fit_catalogue'],
        mode='lines',
        line=dict(color=colour_fill),
        name=catalogue_dict['string_stats_catalogue']))

    basic_style(fig)

    return fig


def add_trace_temporal_bvalue(fig, catalogue_df, b_in_time_df,
                              colour_catalogue, catalogue_name,
                              type='transient', time_column=TIME_COLUMN):
    '''
    Add a trace to the temporal b-value plot
    Args:
        fig: plotly figure object
        catalogue_df: dataframe with catalogue data
        b_in_time_df: dataframe of b-value in time with columns
            'b-value', 'time' and 'std b-value'
        colour_catalogue: colour of the catalogue
        catalogue_name: name of the catalogue
        type: type of trace to add (default='transient',
            can also be 'evolution')
        time_column: name of the time column in the catalogue dataframe,
            default=TIME_COLUMN
    Returns:
        fig: plotly figure object with added trace on 1st y-axis and cumulative
            number of events on 2nd y-axis
    '''
    if colour_catalogue == 'green' and type == 'transient':
        colour_plot = DARK_TLS_COLORS[0]
    elif colour_catalogue == 'green' and type == 'evolution':
        colour_plot = TLS_COLORS[0]
    elif colour_catalogue == 'orange' and type == 'transient':
        colour_plot = DARK_TLS_COLORS[1]
    elif colour_catalogue == 'orange' and type == 'evolution':
        colour_plot = TLS_COLORS[1]
    elif colour_catalogue == 'red' and type == 'transient':
        colour_plot = DARK_TLS_COLORS[2]
    elif colour_catalogue == 'red' and type == 'evolution':
        colour_plot = TLS_COLORS[2]
    elif colour_catalogue == 'blue' and type == 'transient':
        colour_plot = SHADES_MODEL_1[2]
    elif colour_catalogue == 'blue' and type == 'evolution':
        colour_plot = SHADES_MODEL_1[4]
    elif colour_catalogue == 'pink' and type == 'transient':
        colour_plot = SHADES_MODEL_2[2]
    elif colour_catalogue == 'pink' and type == 'evolution':
        colour_plot = SHADES_MODEL_2[4]
    else:
        if type == 'transient':
            colour_plot = 'black'
        else:
            colour_plot = 'grey'

    fig.add_trace(go.Scatter(x=b_in_time_df['time'],
                             y=b_in_time_df['b-value'],
                             error_y=dict(type='data',
                                          array=b_in_time_df['std b-value'],
                                          visible=True),
                             name=type + ' b-value ' + catalogue_name,
                             marker=dict(color=colour_plot)),
                  secondary_y=False)

    fig.add_trace(go.Scatter(x=catalogue_df[time_column],
                             y=np.arange(1, len(catalogue_df) + 1),
                             mode='lines', line=dict(color='#333333'),
                             name='Cumulative number of events (' + catalogue_name + ')'),
                  secondary_y=True)

    basic_style_2yaxes(fig)
    return fig


def plotting_FBG_timeseries(df, mapping, df_hydr=pd.DataFrame()):
    fig = make_subplots(specs=[[{'secondary_y': True}]])

    depth_mapping = mapping["depth"]
    columns = [key[0] for key in depth_mapping.items()]
    df = df[columns]

    column_mapping = {key: f"{value} m" for key,
                      value in depth_mapping.items()}
    df.rename(columns=column_mapping, inplace=True)

    linestyles = DashValidator().values
    linecolors = qualitative.T10

    for idx, column in enumerate(df.columns):
        fig.add_trace(
            go.Scatter(
                x=df.index,  # X-axis: time
                # Y-axis: values for this depth
                y=df[column]-df[column].values[0],
                mode='lines',  # Line plot
                line=dict(color=linecolors[
                    idx % (len(linecolors) - 1)],
                    dash=linestyles[
                    idx % (len(linestyles) - 1)]),

                name=column
            )
        )

    fig.update_yaxes(title_text='<b>Strain [\u03BC\u03B5]</b>')
    fig.update_layout(title='Timeseries')

    if ~df_hydr.empty:
        for idx, column in enumerate(df_hydr.columns):
            if column == "Injection Pressure":
                fig.add_trace(
                    go.Scatter(x=df_hydr.index,
                               y=df_hydr[column],
                               mode='lines',
                               line=dict(
                                   color='rgba(0, 128, 0, 0.3)', width=3),
                               name=column),
                    secondary_y=True)
            elif column == "Pressure Zone":
                fig.add_trace(
                    go.Scatter(x=df_hydr.index,
                               y=df_hydr[column],
                               mode='lines',
                               line=dict(
                                   color='rgba(255, 165, 0, 0.5)', width=3),
                               name=column),
                    secondary_y=True)

        fig.update_yaxes(
            title_text='<b>Pressure [MPa]</b>', secondary_y=True, )

    basic_style(fig)

    return fig


def plotting_FBG_waterfall(df, mapping, factor):

    depth_mapping = mapping["depth"]
    columns = [key[0] for key in depth_mapping.items()]
    df = df[columns]

    column_mapping = {key: f"{value} m" for key,
                      value in depth_mapping.items()}
    df.rename(columns=column_mapping, inplace=True)

    depths = np.array([depth_mapping[d] for d in depth_mapping])
    tmp_FBG_data = df.iloc[:, 0:].values.T  # Transpose to match the structure

    # Normalize the data by subtracting the first column of each row
    tmp_FBG_data_normalized = tmp_FBG_data - tmp_FBG_data[:, 0:1]

    new_tmp_FBG_data = np.concatenate(
        (tmp_FBG_data_normalized, tmp_FBG_data_normalized, tmp_FBG_data_normalized))
    new_depths = np.concatenate((depths, depths, depths))*np.nan

    min_val = -max(abs(np.nanmin(tmp_FBG_data_normalized)),
                   abs(np.nanmax(tmp_FBG_data_normalized)))
    max_val = max(abs(np.nanmin(tmp_FBG_data_normalized)),
                  abs(np.nanmax(tmp_FBG_data_normalized)))

    for i in range(0, len(new_depths), 3):
        j = int(i/3)
        new_tmp_FBG_data[i, :] = np.nan
        new_tmp_FBG_data[i+1, :] = tmp_FBG_data_normalized[j, :]
        new_tmp_FBG_data[i+2, :] = np.nan

        new_depths[i] = depths[j]-factor
        new_depths[i+1] = depths[j]
        new_depths[i+2] = depths[j] + factor

    fig = px.imshow(new_tmp_FBG_data,
                    x=df.index,
                    y=new_depths,  # Use the fake depths
                    zmin=min_val, zmax=max_val,
                    color_continuous_scale='RdBu_r')

    fig.update_yaxes(title_text='<b>Strain [\u03BC\u03B5]</b>')

    fig.update_layout(title='Waterfall plot',
                      yaxis_title='<b>Borehole depth [m]</b>',
                      # Add label to colorbar
                      coloraxis_colorbar_title='<b>Strain [\u03BC\u03B5]</b>',
                      coloraxis_colorbar_title_side="right",  # Position the title at the top
                      # coloraxis_colorbar_title_font = dict(size=14, family='Arial', color='black'),  # Font settings
                      coloraxis_colorbar_ticks="outside",  # Optionally, add ticks outside
                      )

    basic_style(fig)

    return fig

def plotting_SIMFIP_timeseries(df, borehole, mapping, df_hydr=pd.DataFrame()):
    fig = make_subplots(specs=[[{'secondary_y': True}]])

    columns = [key[0] for key in mapping['channels'].items()]
    df = df[columns]

    df.rename(columns=mapping["channels"], inplace=True)

    linestyles = DashValidator().values
    linecolors = qualitative.T10

    for idx, column in enumerate(df.columns):
        fig.add_trace(
            go.Scatter(
                x=df.index,  # X-axis: time
                # Y-axis: values for this depth
                y=df[column]-df[column].values[0],
                mode='lines',  # Line plot
                line=dict(color=linecolors[
                    idx % (len(linecolors) - 1)],
                    dash=linestyles[
                    idx % (len(linestyles) - 1)]),
                name=column  # Legend name (column name, e.g., "7.9m")
            )
        )

    fig.update_yaxes(title_text='<b>displacement [mm]</b>')
    fig.update_layout(title=borehole)

    if ~df_hydr.empty:
        for idx, column in enumerate(df_hydr.columns):
            if column == "Injection Pressure":
                fig.add_trace(
                    go.Scatter(x=df_hydr.index,
                               y=df_hydr[column],
                               mode='lines',
                               line=dict(color='rgba(0, 128, 0, 0.3)', width=3),
                               name=column),
                               secondary_y=True)
            elif column == "Pressure Zone":
                fig.add_trace(
                    go.Scatter(x=df_hydr.index,
                               y=df_hydr[column],
                               mode='lines',
                               line=dict(color='rgba(255, 165, 0, 0.5)', width=3),
                               name=column),
                               secondary_y=True)

        fig.update_yaxes(title_text='<b>Pressure [MPa]</b>', secondary_y=True, )

    basic_style(fig)

    return fig


def plotting_hydraulics_INFLUXDB(df, borehole, mapping):

    columns = [key[0] for key in mapping['channels'].items()]
    df = df[columns]

    maps = {key: value['description']
            for key, value in mapping['channels'].items()}

    df.rename(columns=maps, inplace=True)

    fig = make_subplots(specs=[[{'secondary_y': True}]])
    linestyles = DashValidator().values
    linecolors = qualitative.T10

    for idx, column in enumerate(df.columns):
        if column == "Injection rate":
            DeltaT = (df.index[1] - df.index[0]).total_seconds()
            fig.add_trace(
                go.Scatter(x=df.index,
                           y=df[column],
                           mode='lines',
                           line=dict(color=linecolors[
                               idx % (len(linecolors) - 1)],
                               dash=linestyles[
                               idx % (len(linestyles) - 1)]),
                           name=column+" (Tot. "+str(np.fix(np.nansum(df[column].values/60*DeltaT/1000)*1000)/1000)+" m3)"),
                secondary_y=True)
        else:
            fig.add_trace(
                go.Scatter(x=df.index,
                           y=df[column],
                           mode='lines',
                           line=dict(color=linecolors[
                               idx % (len(linecolors) - 1)],
                               dash=linestyles[
                               idx % (len(linestyles) - 1)]),
                           name=column),
                secondary_y=False)
        fig.update_yaxes(title_text='<b>Flow [l/min]</b>', secondary_y=True)
        fig.update_yaxes(
            title_text='<b>Pressure [MPa]</b>', secondary_y=False, )

    # Set y-axes titles
    fig.update_xaxes(title_text='Time [UTC]')
    fig.update_legends({'x': 0, 'y': 1, 'yanchor': 'top'})
    fig.update_layout(yaxis={'showexponent': 'all', 'exponentformat': 'e'},
                      yaxis2={'showexponent': 'all', 'exponentformat': 'e'})

    basic_style_2yaxes(fig)
    fig.update_layout(
        title=borehole
    )

    return fig
