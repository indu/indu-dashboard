import pandas as pd
import numpy as np
def get_fbg(client,bucket,stations,start,end):
    '''
    Load FBG data for sensors listed in stations
    Args:
        client: InfluxDBClient
        bucket: InfluxDB bucket
        stations: list of measurements points (stations)
        start: starting datetime
        end: ending datetime
    Returns:
        list of sections: list of dataframes
    '''

    query_api = client.query_api()
    startdate = start.strftime("%Y-%m-%dT%H:%M:%SZ")
    enddate = end.strftime("%Y-%m-%dT%H:%M:%SZ")

    output = []

    stations_filter = " or ".join([f'r._measurement == "{s}"' for s in stations])

    query = f"""
    from(bucket:"{bucket}")
    |> range(start: {startdate}, stop: {enddate})
    |> filter(fn: (r) => r.device == "FBG")
    |> filter(fn: (r) => {stations_filter})
    |> aggregateWindow(every: 1m, fn: mean, createEmpty: false)
    |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
    |> keep(columns: ["_time", "value", "_measurement"])
    """

    combined_df = query_api.query_data_frame(query)
    df = combined_df.pivot(index='_time', columns='_measurement', values='value')
    df.reset_index(inplace=True)

    df['datetime'] = pd.to_datetime(
        df['_time'],
        format='%Y-%m-%d %H:%M:%S.%f+00:00')
    df.set_index(['datetime'], inplace=True)
    df.drop(['_time'], axis=1, inplace=True)
    #df.rename(columns=stations, inplace=True)

    missing_columns = set(stations) - set(df.columns)

    for col in missing_columns:
        df[col] = np.nan

    return df

def get_simfip(client,bucket,stations,start,end):
    '''
    Load SIMFIP data for sensors listed in stations
    Args:
        client: InfluxDBClient
        bucker: InfluxDB bucket
        stations: list of measurements points (stations)
        start: starting datetime
        end: ending datetime
    Returns:
        list of sections: list of dataframes
    '''

    query_api = client.query_api()
    startdate = start.strftime("%Y-%m-%dT%H:%M:%SZ")
    enddate = end.strftime("%Y-%m-%dT%H:%M:%SZ")

    output = []

    stations_filter = " or ".join([f'r._measurement == "{s}"' for s in stations])

    #| > filter(fn: (r) = > r.device == "simfip")


    query = f"""
    from(bucket: "{bucket}")
    |> range(start: {startdate}, stop: {enddate})
    |> filter(fn: (r) => {stations_filter})
    |> aggregateWindow(every: 10s, fn: mean, createEmpty: false)
    |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
    |> keep(columns: ["_time", "value", "_measurement"])
    """

    combined_df = query_api.query_data_frame(query)
    df = combined_df.pivot(index='_time', columns='_measurement', values='value')
    df.reset_index(inplace=True)

    df['datetime'] = pd.to_datetime(
        df['_time'],
        format='%Y-%m-%d %H:%M:%S.%f+00:00')
    df.set_index(['datetime'], inplace=True)
    df.drop(['_time'], axis=1, inplace=True)
    #df.rename(columns=stations, inplace=True)

    missing_columns = set(stations) - set(df.columns)

    for col in missing_columns:
        df[col] = np.nan

    return df


