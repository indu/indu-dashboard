from hydws.parser.parser import SectionHydraulics
import pandas as pd
import numpy as np

def get_sections(client,borehole_name,start,end):
    '''
    Load section hydraulics a borehole with borehole_name
    Args:
        client: HYDWS client
        borehole_name: str
        start: starting datetime
        end: ending datetime
    Returns:
        list of sections: list of section hydraulics
    '''
    sections = client.list_section_names(borehole_name)
    section_hydraulics = []

    for s in sections:
        data = client.get_section(borehole_name, s, start, end)
        section_data = SectionHydraulics(data)
        if not section_data.hydraulics.empty:
            section_hydraulics.append(section_data)

    return section_hydraulics


def get_sections_INFLUXDB(client,bucket,stations,scalings,start,end,wind=60):
    '''
    Load section hydraulics a borehole with borehole_name
    Args:
        client: InfluxDBClient
        stations: list of measurements points (stations)
        start: starting datetime
        end: ending datetime
    Returns:
        list of sections: list of dataframes
    '''

    query_api = client.query_api()
    startdate = start.strftime("%Y-%m-%dT%H:%M:%SZ")
    enddate = end.strftime("%Y-%m-%dT%H:%M:%SZ")

    output = []

    stations_filter = " or ".join([f'r._measurement == "{s}"' for s in stations])

    #| > filter(fn: (r) = > r.device == "simfip")

    query = f"""
    from(bucket: "{bucket}")
    |> range(start: {startdate}, stop: {enddate})
    |> filter(fn: (r) => {stations_filter})
    |> aggregateWindow(every: {wind}s, fn: mean, createEmpty: false)
    |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
    |> keep(columns: ["_time", "value", "_measurement"])
    """

    combined_df = query_api.query_data_frame(query)

    station_to_scaling = {station: scalings[i] for i, station in enumerate(stations)}

    def apply_scaling(row):
        scaling_factor = station_to_scaling.get(row['_measurement'], 1)
        return row['value'] * scaling_factor

    combined_df['scaled_value'] = combined_df.apply(apply_scaling, axis=1)

    df = combined_df.pivot(index='_time', columns='_measurement', values='scaled_value')
    df.reset_index(inplace=True)

    df['datetime'] = pd.to_datetime(
        df['_time'],
        format='%Y-%m-%d %H:%M:%S.%f+00:00')
    df.set_index(['datetime'], inplace=True)
    df.drop(['_time'], axis=1, inplace=True)
    #df.rename(columns=stations, inplace=True)

    missing_columns = set(stations) - set(df.columns)

    for col in missing_columns:
        df[col] = np.nan

    return df


